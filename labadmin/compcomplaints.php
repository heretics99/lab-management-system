<?
include("checksession.php");
include("dbconnect.php");
connect();
include("config.php");

$orderby=$_GET['orderby'];	
$direction=$_GET['direction'];
$newdir="ASC";
if($direction=='asc'||$direction=='ASC') {$direction="ASC"; $newdir="DESC";}
if($direction=='desc'||$direction=='DESC') {$direction="DESC"; $newdir="ASC";}

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Computer Complaints</title>
<link rel="stylesheet" type="text/css" href="style.css"> 
<script language="javascript" src="checkbox.js"></script>
<script language="javascript" src="printfunction.js"></script>
</head>

<body>
<?php include("left.php");?> 
<div id="windowContent">

<p align="center"><b><span style="font-size: 20pt">Computer Complaints</span></b></p>

<?

$query = "SELECT * FROM comp_complaint WHERE status LIKE 'pending'";
$result = mysql_query($query);
$row = mysql_fetch_array($result);

if(!$row) echo "no computer complaint pending";

else
{
?>
<p><span style="font-size: 13pt"><b><u>Computer Complaints</u></b></span></p>
<form name="compcomplaints" id="compcomplaints" action="compcomplaintsmsg.php" method="POST">
<input type='hidden' name='stno' id='stno'>
<input type='hidden' name='app' id='app'>
<table border="2" cellpadding="5" cellspacing="1" style="border-collapse: collapse" bordercolor="#999999" width="200%">
  <tr>
	<td width="2%"><b>Sno.</b></td>
	<td width="5%"><b><a  title="sort Id by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'compcomplaints.php?orderby=id&direction='.$newdir?>"> Id</a></b></td>
    <td width="10%"><b><a  title="sort Student No by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'compcomplaints.php?orderby=comp_complaint.Student_No&direction='.$newdir?>">By Student No</a></b></td>
    <td width="15%"><b><a  title="sort Student Name by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'compcomplaints.php?orderby=Name&direction='.$newdir?>">Student Name</a></b></td>
	<td width="15%"><b><a  title="sort Lab by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'compcomplaints.php?orderby=Lab&direction='.$newdir?>">Lab</a></b></td>
	<td width="3%"><b><a  title="sort PC No by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'compcomplaints.php?orderby=Computer_No&direction='.$newdir?>">PC No</a></b></td>
	<td width="35%"><b><a  title="sort Complaint by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'compcomplaints.php?orderby=Complaint&direction='.$newdir?>">Complaint</a></b></td>
	<td width="5%"><b><a  title="sort Date of Complaint by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'compcomplaints.php?orderby=dateval&direction='.$newdir?>">Date of Complaint</a></b></td>
	<td width="10%" colspan="2"><p align="center"><b>Action</b></td>
  </tr>

  <?
  $sorting="";
  if($orderby!="" && $direction!="")  $sorting=" ORDER BY $orderby $direction";
    
  $query2 = "SELECT * FROM comp_complaint INNER JOIN stu_list ON comp_complaint.Student_No = stu_list.Student_No WHERE comp_complaint.status LIKE 'pending' $sorting";
  $result2 = mysql_query($query2);
  ?>
  <tr>
  <?
  $i=0;
  while ($row2 = mysql_fetch_array($result2)) 
  {
  $i++;
  ?>
  <td><input type="checkbox" name="list[]" id="list[]" value="<?echo $row2['id']?>"><?echo $i?></td>
  <td><?echo $row2['id']?></td>
  <td><a title='view student details' href='perticularstudetails.php?stno=<?echo $row2['Student_No']?>'><?echo $row2['Student_No']?></td>
  <td><?echo $row2['Name']?></td>
  <td><?echo $row2['Lab']?></td>
  <td><?echo $row2['Computer_No']?></td>
  <td><?echo $row2['Complaint']?></td>
  <td><?echo date('F d, Y \a\t h:i:s a',strtotime($row2['dateval']))?></td>
  <td><?echo "<input name='resolve' value='resolve' type='button' onClick=\"if(confirm('Press OK only after you have resolved the problem on computer number \'".$row2['Computer_No']."\' in lab \'".$row2['Lab']."\' made on \'".date('F d, Y \a\t h:i:s a',strtotime($row2['dateval']))."\' by \'".$row2['Name']."\' with student number \'".$row2['Student_No']."\' .')) {document.getElementById('stno').value='".$row2['id']."';document.getElementById('app').value='approve';document.getElementById('compcomplaints').submit();}\"/>";
?></td>

  <td><?echo "<input name='dismiss' value='dismiss' type='button' onClick=\"if(confirm('are you sure you wish to dismiss complaint id:\'".$row2['id']."\' ? ')) {document.getElementById('stno').value='".$row2['id']."';document.getElementById('app').value='dismiss';document.getElementById('compcomplaints').submit();}\"/>";
?></td>

  
  </tr>
  
<?
} //end of while
?>
</table>
<br>
<input type="button" name="CheckAll" value="Check All"
onClick="checkAll(document.compcomplaints['list[]'])">
<input type="button" name="UnCheckAll" value="Uncheck All"
onClick="uncheckAll(document.compcomplaints['list[]'])">

<i>with selected: </i>

<select name="submit_mult" id="submit_mult">
    <option value="approvegrp">Resolve </option>
    <option value="dismissgrp">Dismiss </option>
    </select>
<input type="button" name="go" value="Go" onClick='if(countChecks(this.form)>0) {if(confirm("are you sure you want to \""+document.getElementById("submit_mult").options[document.getElementById("submit_mult").selectedIndex].text+" "+countChecks(this.form)+" items selected"+"\"")) {document.getElementById("app").value=document.getElementById("submit_mult").options[document.getElementById("submit_mult").selectedIndex].value;document.getElementById("compcomplaints").submit();}} else alert("you have not selected any item!!")' >
<br>


<br>


</form>
<br>
<input type="button" value="Print Report" name="Home" onClick="PrintContent()">

<?
} //end of if


?>
</div>

</body>
</html>


