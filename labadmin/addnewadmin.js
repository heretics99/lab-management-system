function formValidator(){
//alert(document.getElementById('batch_year').options[document.getElementById('batch_year').selectedIndex].value);
//return false
	// Make quick references to our fields
	var username = document.getElementById('username');
	var pass1 = document.getElementById('pass1');
	var pass2 = document.getElementById('pass2');
	var permission = document.getElementById('permission');
	var name = document.getElementById('name');
	var empno = document.getElementById('empno');
	var pno=document.getElementById('pno');
	var email=document.getElementById('email');
	
	// Check each input in the order that it appears in the form!
	if(isAlphanumeric(username, "Please enter only alphanumeric characters for Admin Name")){
		if(lengthRestriction(pass1,"password", 4, 50)){
			if(pass1.value==pass2.value){
				if(isNumeric(permission, "Please enter a 1 digit number")){
					if(lengthRestriction(permission, "permission",1,1)){					
						if(isAlphabet(name, "Please enter only letters for your name")){
							if(notEmpty(empno, "Please enter employee Number")){
								if(isNumeric(pno, "Please enter a valid 10 digit phone number")){
									if(lengthRestriction(pno, "phone number",10,10)){
										if(emailValidator(email, "Please enter a valid email address")){
											return true;
										}
									}
								}
							}
						}
					}
				}
			}
		else {
		alert("both the passwords should match")
		document.getElementById('pass2').focus()
		}
		}
	}
				
		return false;	
	
}

function notEmpty(elem, helperMsg){
	if(elem.value.length == 0){
		alert(helperMsg);
		elem.focus(); // set the focus to this input
		return false;
	}
	return true;
}

function isNumeric(elem, helperMsg){
	var numericExpression = /^[0-9]+$/;
	if(elem.value.match(numericExpression)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-zA-Z\s]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphanumeric(elem, helperMsg){
	var alphaExp = /^[0-9a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function lengthRestriction(elem,name, min, max){
	var uInput = elem.value;
	if(uInput.length >= min && uInput.length <= max){
		return true;
	}else{
		alert("Please enter "+name+" between " +min+ " and " +max+ " characters");
		elem.focus();
		return false;
	}
}

function madeSelection(elem, helperMsg){
	if(elem.value == "--Please select a lab--"){
		alert(helperMsg);
		elem.focus();
		return false;
	}else{
		return true;
	}
}

function emailValidator(elem, helperMsg){
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(elem.value.match(emailExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}


