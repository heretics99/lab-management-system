<?php
session_start();
if (!isset($_SESSION['is_logged_in'])||!isset($_SESSION['session_timeout_start'])) {
	 header("Location:index.php");
     die();     // just to make sure no scripts execute
}


if(!isset($_SESSION['session_timeout_start'])) {
             
             $_SESSION['session_timeout_start']=time();			 
       }  
else
{
		$session_timeout = 1800; // 30 minutes (in sec)
       $session_duration = time() - $_SESSION['session_timeout_start'];
       if ($session_duration > $session_timeout) {
           
           unset($_SESSION['is_logged_in']);
		   unset($_SESSION['session_timeout_start']);
		   unset($_SESSION['adminuser_logged_in']);
		   if(isset($_SESSION['super_is_logged_in'])) unset($_SESSION['super_is_logged_in']);
		   
           
               header("Location: index.php?expired=yes");  // Redirect to Login Page
       } 
	   else {       
           $_SESSION['session_timeout_start']=time();
		   //echo $_SESSION['adminuser_logged_in'];
       }
}
?>