<?
include("checksession.php");
include("dbconnect.php");
connect();
include("config.php");

$orderby=$_GET['orderby'];	
$direction=$_GET['direction'];
$newdir="ASC";
if($direction=='asc'||$direction=='ASC') {$direction="ASC"; $newdir="DESC";}
if($direction=='desc'||$direction=='DESC') {$direction="DESC"; $newdir="ASC";}

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Stationary requests notifications</title>
<link rel="stylesheet" type="text/css" href="style.css"> 
<script language="javascript" src="checkbox.js"></script>
<script language="javascript" src="printfunction.js"></script>
</head>

<body>
<?php include("left.php");?> 
<div id="windowContent">

<p align="center"><b><span style="font-size: 20pt">Stationary requests notifications</span></b></p>

<?

$query = "SELECT * FROM stu_stationary WHERE approval LIKE 'unapproved'";
$result = mysql_query($query);
$row = mysql_fetch_array($result);

if(!$row) echo "no new stationary request pending";

else
{
?>
<p><span style="font-size: 13pt"><b><u>Stationary requests notifications</u></b></span></p>
<form name="stationaryrequests" id="stationaryrequests" action="stationaryrequestsmsg.php" method="POST">
<input type='hidden' name='stno' id='stno'>
<input type='hidden' name='app' id='app'>
<table border="2" cellpadding="5" cellspacing="1" style="border-collapse: collapse" bordercolor="#999999" width="150%">
  <tr>
	<td width="15"><b>Sno.</b></td>
	<td width="20"><b><a  title="sort Request Id by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'stationaryrequests.php?orderby=id&direction='.$newdir?>">Request Id</a></b></td>
    <td width="30"><b><a  title="sort Student No by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'stationaryrequests.php?orderby=stu_stationary.Student_No&direction='.$newdir?>">Student No</a></b></td>
    <td width="150"><b><a  title="sort Name by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'stationaryrequests.php?orderby=Name&direction='.$newdir?>">Name</a></b></td>
	<!--<td width="25"><b><a  title="sort Gender by <?echo $newdir=='ASC'?'ascending':'descending'?> order"  href="<?echo $base.'stationaryrequests.php?orderby=Gender&direction='.$newdir?>">Gender</a></b></td>
    <td width="20"><b><a  title="sort Branch by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'stationaryrequests.php?orderby=Branch&direction='.$newdir?>">Branch</a></b></td>
	<td width="20"><b><a  title="sort Batch by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'stationaryrequests.php?orderby=Batch&direction='.$newdir?>">Batch</a></b></td>-->
    <td width="100"><b><a title="sort Pages Requested by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'stationaryrequests.php?orderby=Pg_Requested&direction='.$newdir?>">Pages Asked</a></b></td>
	<td width="150"><b><a  title="sort Request Date by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'stationaryrequests.php?orderby=dateval&direction='.$newdir?>">Request Date</a></b></td>
	<td width="130" colspan="2"><p align="center"><b>Action</b></td>
  </tr>

  <?
  $sorting="";
  if($orderby!="" && $direction!="")  $sorting=" ORDER BY $orderby $direction";
    
  $query2 = "SELECT * FROM stu_stationary INNER JOIN stu_list ON stu_stationary.Student_No = stu_list.Student_No WHERE stu_stationary.approval LIKE 'unapproved' $sorting";
  $result2 = mysql_query($query2);
  ?>
  <tr>
  <?
  $i=0;
  while ($row2 = mysql_fetch_array($result2)) 
  {
  $i++;
  ?>
  <td><input type="checkbox" name="list[]" id="list[]" value="<?echo $row2['id']?>"><?echo $i?></td>
  <td><?echo $row2['id']?></td>
  <td><a title='view student details' href='perticularstudetails.php?stno=<?echo $row2['Student_No']?>'><?echo $row2['Student_No']?></td>
  <td><?echo $row2['Name']?></td>
  <!--<td><?echo $row2['Gender']?></td>
  <td><?echo $row2['Branch']?></td>
  <td><?echo $row2['Batch']?></td>-->
  <td><?echo $row2['Pg_Requested']?></td>
  <td><?echo date('h:ia, d-M-y',strtotime($row2['dateval']))?></td>
  <td><?echo "<input name='approve' value='approve' type='button' onClick=\"if(confirm('are you sure you wish to approve \'".$row2['Pg_Requested']."\' pages request by \'".$row2['Name']."\' with student number \'".$row2['Student_No']."\' ? PRESS OK ONLY AFTER YOU GIVE PAGES TO STUDENT.')) {document.getElementById('stno').value='".$row2['id']."';document.getElementById('app').value='approve';document.getElementById('stationaryrequests').submit();}\"/>";
?></td>
  <td><?echo "<input name='dismiss' value='dismiss' type='button' onClick=\"if(confirm('are you sure you wish to dismiss \'".$row2['Pg_Requested']."\'pages requested by \'".$row2['Name']."\' with student number \'".$row2['Student_No']."\' ? ')) {document.getElementById('stno').value='".$row2['id']."';document.getElementById('app').value='dismiss';document.getElementById('stationaryrequests').submit();}\"/>";
?></td>
  
  </tr>
  
<?

} //end of while
?>
</table>
<br>
<input type="button" name="CheckAll" value="Check All"
onClick="checkAll(document.stationaryrequests['list[]'])">
<input type="button" name="UnCheckAll" value="Uncheck All"
onClick="uncheckAll(document.stationaryrequests['list[]'])">

<i>with selected: </i>

<select name="submit_mult" id="submit_mult">
    <option value="approvegrp">Approve </option>
    <option value="dismissgrp">Dismiss </option>
    </select>
<input type="button" name="go" value="Go" onClick='if(countChecks(this.form)>0) {if(confirm("are you sure you want to \""+document.getElementById("submit_mult").options[document.getElementById("submit_mult").selectedIndex].text+" "+countChecks(this.form)+" items selected"+"\"")) {document.getElementById("app").value=document.getElementById("submit_mult").options[document.getElementById("submit_mult").selectedIndex].value;document.getElementById("stationaryrequests").submit();}} else alert("you have not selected any item!!")' >
<br>


<br>

</form>
<br>
<input type="button" value="Print Report" name="Home" onClick="PrintContent()">

<?
} //end of if


?>
</div>

</body>
</html>


