<?
include("checksession.php");
include("dbconnect.php");
connect();
include("config.php");

$orderby=$_GET['orderby'];	
$direction=$_GET['direction'];
$newdir="ASC";
if($direction=='asc'||$direction=='ASC') {$direction="ASC"; $newdir="DESC";}
if($direction=='desc'||$direction=='DESC') {$direction="DESC"; $newdir="ASC";}

?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Student logins today</title>
<link rel="stylesheet" type="text/css" href="style.css"> 
<script language="javascript" src="printfunction.js"></script>
</head>

<body>
<?php include("left.php");?> 
<div id="windowContent">

<p align="center"><b><span style="font-size: 20pt">Student logins today</span></b></p>

<?
date_default_timezone_set ("Asia/Calcutta");
$nowdate=date("Y-m-d 00:00:00", time());

$query = "SELECT *,`stu_list`.`Student_No` FROM stu_list INNER JOIN (SELECT Student_No,In_Time,Out_Time,(Out_Time-In_Time) elapsed,Lab_ID,Computer_No FROM `stu_entry` WHERE `stu_entry`.`In_Time` >= '$nowdate') intime ON `intime`.`Student_No`=`stu_list`.`Student_No`";
$result = mysql_query($query);
$row = mysql_fetch_array($result);

if(!$row) echo "No student logins today.";

else
{
$query = "SELECT DISTINCT Lab_ID FROM `lab_active`";
$result = mysql_query($query);
$row = mysql_fetch_array($result);

?>
<p><span style="font-size: 13pt"><b><u>Student logins today</u></b></span></p>
<?
$query2 = "SELECT * FROM `lab_info` WHERE Lab_ID='".$row['Lab_ID']."'";
$result2 = mysql_query($query2);
$row2 = mysql_fetch_array($result2);

echo "<h4>The active lab is : <u>".$row2['Lab_Name']."</u> (Lab Id:".$row['Lab_ID'].")</h4>";
?>

<form name="stulogintoday" id="stulogintoday" action="stulogintodaymsg.php" method="POST">
<input type='hidden' name='stno' id='stno'>
<input type='hidden' name='app' id='app'>
<input type='hidden' name='reason' id='reason'>
<table border="2" cellpadding="5" cellspacing="1" style="border-collapse: collapse" bordercolor="#999999" width="950">
  <tr>
	<td width="10"><b>Sno.</b></td>
	<td width="120"><b><a title="sort Name by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'stulogintoday.php?orderby=Name&direction='.$newdir?>">Name</a></b></td>
	<td width="30"><b><a title="sort Student No. by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'stulogintoday.php?orderby=stu_list.Student_No&direction='.$newdir?>">Student No</a></b></td>
    <td width="25"><b><a title="sort Gender by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'stulogintoday.php?orderby=Gender&direction='.$newdir?>">Gender</a></b></td>
	<td width="20"><b><a title="sort Phone Number. by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'stulogintoday.php?orderby=Phone_Number&direction='.$newdir?>">Phone Number</a></b></td>
	<td width="100"><b><a title="sort Login Time by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'stulogintoday.php?orderby=In_Time&direction='.$newdir?>">Login time</a></b></td>
	<td width="100"><b><a title="sort Logout Time by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'stulogintoday.php?orderby=Out_Time&direction='.$newdir?>">Logout time</a></b></td>
	<td width="150"><b><a title="sort Elapsed Time by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'stulogintoday.php?orderby=elapsed&direction='.$newdir?>">Elapsed time</a></b></td>
	<td width="50"><b><a title="sort System No by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'stulogintoday.php?orderby=Computer_No&direction='.$newdir?>">System No</a></b></td>
	<td width="130" colspan="2"><p align="center"><b>Action</b></td>
  </tr>

  <?
  $sorting="";
  if($orderby!="" && $direction!="")  $sorting=" ORDER BY $orderby $direction";
    
  $query2 = "SELECT *,`stu_list`.`Student_No` FROM stu_list INNER JOIN 
  (SELECT Student_No,In_Time,Out_Time,
  (unix_timestamp(Out_Time) - unix_timestamp(In_Time)) elapsed,
  Lab_ID,Computer_No FROM `stu_entry` WHERE `stu_entry`.`In_Time` >= '$nowdate') 
  intime ON `intime`.`Student_No`=`stu_list`.`Student_No` $sorting";
  //	print $query2;
  $result2 = mysql_query($query2);
  echo mysql_error();
  ?>
  <tr>
  <?
  $i=0;
  //echo time()."<br>";
  while ($row2 = mysql_fetch_array($result2)) 
  {
  $i++;
  $elapsed=$row2['elapsed'];
  //echo $row2['In_Time']."    ";
  if($elapsed<0) $elapsed=time()-strtotime($row2['In_Time']);
  
  //echo $elapsed."<br>";
   $days=floor($elapsed / 84600);
   $elapsed -= 84600 * floor($elapsed / 84600);

   $hours=floor($elapsed / 3600);
   $elapsed -= 3600 * floor($elapsed / 3600);
  // echo $hours;
   $minutes = floor($elapsed / 60);
   $elapsed -= 60 * floor($elapsed / 60);
   
   $seconds=$elapsed;
   //echo ' minutes, and'. $elapsed.' seconds ago.';
   
  
  ?>
  <td><?echo $i?></td>
  <td><?echo $row2['Name']?></td>
  <td><a title='view student details' href='perticularstudetails.php?stno=<?echo $row2['Student_No']?>'><?echo $row2['Student_No']?></td>
  <td><?echo $row2['Gender']?></td>
  <td><?echo $row2['Phone_Number']?></td>
  <td><?echo date('h:i:sa, d-m-y',strtotime($row2['In_Time']))?></td>
  <td><?echo $row2['Out_Time']=="0000-00-00 00:00:00"?"-":date('h:i:sa, d-m-y',strtotime($row2['Out_Time']))?></td>
  <td><?
  if($days>0) $timeelap= "$days days, $hours hours, $minutes minutes, and $seconds seconds";
  elseif($hours>0) $timeelap= "$hours hours, $minutes minutes, and $seconds seconds";
  elseif($minutes>0) $timeelap= "$minutes minutes, and $seconds seconds";
  elseif($seconds>=0) $timeelap= "$seconds seconds";?>
  <?echo $timeelap;?>
  </td>
  <td><a title='view computer details' href='perticularcompdetails.php?comp=<?echo $row2['Computer_No']?>&orderby=In_Time&direction=DESC'><?echo "<b>".$row2['Computer_No']."</b>"?></a></td>
  <td><p align="center">
  <?
  
  echo $row2['Out_Time']=="0000-00-00 00:00:00"? "<input title='Remotely logout user' name='Logout' value='Logout' type='button' onClick=\"if(confirm('Press OK if you wish to remotely logout student no. \'".$row2['Student_No']."\' sitting on computer No. \'".$row2['Computer_No']."\'.'+'\\n\\n'+'*It is advised that you also BAN the student for not logging out from the lab. It would make them realise their mistake and make them more careful for next time.')) {document.getElementById('stno').value='".$row2['Student_No']."';document.getElementById('app').value='logout';document.getElementById('stulogintoday').submit();}\"/>":"";
  ?>
  </td>
  <td><p align="center">
  <?
  echo $row2['Out_Time']=="0000-00-00 00:00:00"?"<input name='Logout & Ban' value='Logout & Ban' title=\"Logout and Ban '".$row2['Name']."'\" type='button' onClick=\"if(confirm('Press OK if you wish to remotely logout and BAN student no. \'".$row2['Student_No']."\' sitting on computer No. \'".$row2['Computer_No']."\'. *The following reason for ban would be automatially added:'+'\\n\\n'+'\'Failure to logout from Computer No ".$row2['Computer_No']." for $timeelap duration, on ".date('h:i:s a, d-m-y',strtotime($row2['In_Time']))."\'.  ')) {document.getElementById('stno').value='".$row2['Student_No']."';document.getElementById('reason').value='Failure to logout from Computer No ".$row2['Computer_No']." for $timeelap duration, on ".date('h:i:s a, d-m-y',strtotime($row2['In_Time']))."';document.getElementById('app').value='logoutandban';document.getElementById('stulogintoday').submit();}\"/>":"";
?></td>
  
  </tr>
  
<?


	






} //end of while
?>
</table>
</form>
<br>
<input type="button" value="Print Report" name="Home" onClick="PrintContent()">

<?
} //end of if


?>
</div>

</body>
</html>


