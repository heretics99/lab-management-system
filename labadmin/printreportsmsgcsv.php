<?
include("checksession.php");
include("dbconnect.php");
connect();
include("config.php");

$orderby=$_GET['orderby'];	
$direction=$_GET['direction'];
$newdir="ASC";
if($direction=='asc'||$direction=='ASC') {$direction="ASC"; $newdir="DESC";}
if($direction=='desc'||$direction=='DESC') {$direction="DESC"; $newdir="ASC";}

$valid=true;
$startdate1 = $_REQUEST['startdate'];
$enddate1 = $_REQUEST['enddate'];

$arr=split("/",$startdate1); // splitting the array
$mm=$arr[0]; // first element of the array is month
$dd=$arr[1]; // second element is date
$yy=$arr[2]; // third element is year
if(!@checkdate($mm,$dd,$yy))
	{$valid = false;}

$arr=split("/",$enddate1); // splitting the array
$mm=$arr[0]; // first element of the array is month
$dd=$arr[1]; // second element is date
$yy=$arr[2]; // third element is year
if(!@checkdate($mm,$dd,$yy))
	$valid = false;

$startdate = date("Y-m-d", strtotime($startdate1));
$enddate = date("Y-m-d", strtotime($enddate1));
$enddateactual = date("Y-m-d", strtotime(date("Y-m-d", strtotime($enddate1)) . " +1 day"));

$limit= 5000;

if(!$valid) die("no student entry in the mentioned period");
else
{

  $sorting="";
  if($orderby!="" && $direction!="")  $sorting=" ORDER BY $orderby $direction";
  $query = "SELECT @rownum := @rownum + 1 as sno, `stu_list`.`Student_No`,`Name`,Gender,
        In_Time,Out_Time,elapsed,Computer_No FROM (SELECT @rownum := 0) r,
        stu_list INNER JOIN (SELECT Student_No,In_Time,Out_Time,
		(unix_timestamp(Out_Time) - unix_timestamp(In_Time)) elapsed,Lab_ID,Computer_No FROM `stu_entry`
        WHERE (`stu_entry`.`In_Time` >= '$startdate 00:00:00' AND `stu_entry`.`In_Time` <= '$enddateactual 00:00:00')) intime ON `intime`.`Student_No`=`stu_list`.`Student_No` $sorting LIMIT $limit";
  
  $result = mysql_query($query);
  $total = mysql_num_rows($result);
  //echo $query2;
  

	/*for ($i = 0; $i < $num_fields; $i++) 
	{
		$headers[] = mysql_field_name($result , $i);
	}*/
	
	
	$fp = fopen('php://output', 'w');
	if ($fp && $result) 
	{
		header('Content-Type: text/csv');
		$FileName = date("d-m-y-h-i-sa") . '.csv';
		header("Content-Disposition: attachment; filename=$FileName");
		header('Pragma: no-cache');
		header('Expires: 0');
		
		$text=array("Student Entries between ".date("d-m-Y", strtotime($startdate))." and ".date("d-m-Y", strtotime($enddate)));
		fputcsv($fp, $text);
		$text=array("Total $total Student Logins");
		fputcsv($fp, $text);
		$headers = array("Sno.","Name","Student No","Gender","Login time","Logout time","Elapsed time","System No");
		fputcsv($fp, $headers);
		
		$i=0;
		//echo time()."<br>";
		while ($row = mysql_fetch_array($result)) 
		{
			$i++;
			$elapsed=$row['elapsed'];
			//echo $row['In_Time']."    ";
			if($elapsed<0) $elapsed=time()-strtotime($row['In_Time']);

			//echo $elapsed."<br>";
			$days=floor($elapsed / 84600);
			$elapsed -= 84600 * floor($elapsed / 84600);

			$hours=floor($elapsed / 3600);
			$elapsed -= 3600 * floor($elapsed / 3600);
			// echo $hours;
			$minutes = floor($elapsed / 60);
			$elapsed -= 60 * floor($elapsed / 60);

			$seconds=$elapsed;
			//echo ' minutes, and'. $elapsed.' seconds ago.';
	
			$intimedisp=date('d-m-Y, h:i:sa',strtotime($row['In_Time']));
			$outtimedisp=$row['Out_Time']=="0000-00-00 00:00:00"?"-":date('d-m-Y, h:i:sa',strtotime($row['Out_Time']));
			if($days>0) $timeelap= "$days days, $hours hrs, $minutes mins, and $seconds secs";
			elseif($hours>0) $timeelap= "$hours hrs, $minutes mins, and $seconds secs";
			elseif($minutes>0) $timeelap= "$minutes mins, and $seconds secs";
			elseif($seconds>=0) $timeelap= "$seconds secs";
			
			$values = array($i,$row['Name'],$row['Student_No'],$row['Gender'],$intimedisp,$outtimedisp,$timeelap,$row['Computer_No']);

			fputcsv($fp, $values);
		}
			
		echo $fp;
	}
}
  ?>