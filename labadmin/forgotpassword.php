<?
include("checksession.php");
include("dbconnect.php");
connect();
include("config.php");

$orderby=$_GET['orderby'];	
$direction=$_GET['direction'];
$newdir="ASC";
if($direction=='asc'||$direction=='ASC') {$direction="ASC"; $newdir="DESC";}
if($direction=='desc'||$direction=='DESC') {$direction="DESC"; $newdir="ASC";}

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Password Reset requests</title>
<link rel="stylesheet" type="text/css" href="style.css"> 
<script language="javascript" src="printfunction.js"></script>
</head>

<body>
<?php include("left.php");?> 
<div id="windowContent">

<p align="center"><b><span style="font-size: 20pt">Password Reset requests</span></b></p>

<?

$query = "SELECT * FROM stu_list WHERE fp = 1";
$result = mysql_query($query);
$row = mysql_fetch_array($result);

if(!$row) echo "no forgot password requests pending";

else
{
?>
<p><span style="font-size: 13pt"><b><u>Password Reset requests</u></b></span></p>
<form name="newstu" id="newstu" action="fprequest.php" method="POST">
<input type='hidden' name='stno' id='stno'>
<input type='hidden' name='fp' id='fp'>
<table border="2" cellpadding="5" cellspacing="1" style="border-collapse: collapse" bordercolor="#999999" width="100%">
  <tr>
	<td width="10"><b>Sno.</b></td>
    <td width="30"><b><a  title="sort Student No by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'forgotpassword.php?orderby=Student_No&direction='.$newdir?>">Student No</a></b></td>
    <td width="150"><b><a  title="sort Name by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'forgotpassword.php?orderby=Name&direction='.$newdir?>">Name</a></b></td>
	<td width="25"><b><a  title="sort Gender by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'forgotpassword.php?orderby=Gender&direction='.$newdir?>">Gender</a></b></td>
    <td width="20"><b><a  title="sort Branch by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'forgotpassword.php?orderby=Branch&direction='.$newdir?>">Branch</a></b></td>
	<td width="20"><b><a title="sort Batch by <?echo $newdir=='ASC'?'ascending':'descending'?> order"  href="<?echo $base.'forgotpassword.php?orderby=Batch&direction='.$newdir?>">Batch</a></b></td>
    <td width="100"><b><a  title="sort Phone No by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'forgotpassword.php?orderby=Phone_Number&direction='.$newdir?>">Phone No</a></b></td>
	<td width="100"><b><a  title="sort Password Reset Request Date by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'forgotpassword.php?orderby=fpdate&direction='.$newdir?>">Password Reset Request Date</a></b></td>
	<td width="130" colspan="2"><p align="center"><b>Action</b></td>
  </tr>

  <?
  $sorting="";
  if($orderby!="" && $direction!="")  $sorting=" ORDER BY $orderby $direction";
  
  $query2 = "SELECT * FROM `stu_list` WHERE `fp` = 1 $sorting";
  $result2 = mysql_query($query2);
  ?>
  <tr>
  <?
  $i=0;
  while ($row2 = mysql_fetch_array($result2)) 
  {
  $i++;
  ?>
  <td><?echo $i?></td>
  <td><a title='view student details' href='perticularstudetails.php?stno=<?echo $row2['Student_No']?>'><?echo $row2['Student_No']?></td>
  <td><?echo $row2['Name']?></td>
  <td><?echo $row2['Gender']?></td>
  <td><?echo $row2['Branch']?></td>
  <td><?echo $row2['Batch']?></td>
  <td><?echo $row2['Phone_Number']?></td>
  <td><?echo date('d-F-Y, h:i:s a',strtotime($row2['fpdate']))?></td>
  <td><?echo "<input name='reset' value='reset password' type='button' onClick=\"if(confirm('are you sure you wish to reset password for student \'".$row2['Name']."\' with student number \'".$row2['Student_No']."\' ? ')) {document.getElementById('stno').value='".$row2['Student_No']."';document.getElementById('fp').value='reset';document.getElementById('newstu').submit();}\"/>";
?> </td>
  <td><?echo "<input name='dismiss' value='dismiss' type='button' onClick=\"if(confirm('are you sure you wish to dismiss password reset request by \'".$row2['Name']."\' with student number \'".$row2['Student_No']."\' ? ')) {document.getElementById('stno').value='".$row2['Student_No']."';document.getElementById('fp').value='dismiss';document.getElementById('newstu').submit();}\"/>";
?></td>
  
  </tr>
  
<?


	






} //end of while
?>
</table>
</form>
<br>
<input type="button" value="Print Report" name="Home" onClick="PrintContent()">

<?
} //end of if


?>
</div>

</body>
</html>


