<?
include("checksession.php");
include("dbconnect.php");
connect();
include("config.php");

$orderby=$_GET['orderby'];	
$direction=$_GET['direction'];
$newdir="ASC";
if($direction=='asc'||$direction=='ASC') {$direction="ASC"; $newdir="DESC";}
if($direction=='desc'||$direction=='DESC') {$direction="DESC"; $newdir="ASC";}

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Student List</title>
<link rel="stylesheet" type="text/css" href="style.css"> 
<style type="text/css">
	#windowContent{	/* Normal text content */
		float:left;	/* Firefox - to avoid blank white space above panel */
		padding-left:10px;	/* A little space at the left */
	}	
</style>
<script language="javascript" src="printfunction.js"></script>
</head>

<body>
<?php include("left.php");?> 
<div id="windowContent">

<p align="center"><b><span style="font-size: 20pt">Student List</span></b></p>

<?

$query = "SELECT * FROM stu_list";
$result = mysql_query($query);
$row = mysql_fetch_array($result);

if(!$row) echo "no students in database";

else
{
?>
<p><span style="font-size: 13pt"><b><u>Student List</u></b></span></p>
<form name="stulist" id="stulist" action="stulistmsg.php" method="POST">
<input type='hidden' name='stno' id='stno'>
<input type='hidden' name='app' id='app'>
<table border="2" cellpadding="5" cellspacing="1" style="border-collapse: collapse" bordercolor="#999999" width="100%">
  <tr>
	<td width="10"><b>Sno.</b></td>
	<td width="150"><b><a href="<?echo $base.'stulist.php?orderby=Name&direction='.$newdir?>">Name</a></b></td>
	<td width="30"><b><a href="<?echo $base.'stulist.php?orderby=stu_list.Student_No&direction='.$newdir?>">Student No</a></b></td>
    <td width="25"><b><a href="<?echo $base.'stulist.php?orderby=Gender&direction='.$newdir?>">Gender</a></b></td>
	<td width="25"><b><a href="<?echo $base.'stulist.php?orderby=Course&direction='.$newdir?>">Course</a></b></td>
    <td width="20"><b><a href="<?echo $base.'stulist.php?orderby=Branch&direction='.$newdir?>">Branch</a></b></td>
	<td width="20"><b><a href="<?echo $base.'stulist.php?orderby=Batch&direction='.$newdir?>">Batch</a></b></td>
	<td width="20"><b><a href="<?echo $base.'stulist.php?orderby=Phone_Number&direction='.$newdir?>">Phone Number</a></b></td>
	<td width="20"><b><a href="<?echo $base.'stulist.php?orderby=Email&direction='.$newdir?>">Email</a></b></td>
	<td width="20"><b><a href="<?echo $base.'stulist.php?orderby=Permitted&direction='.$newdir?>">Permitted</a></b></td>
	<td width="20"><b><a href="<?echo $base.'stulist.php?orderby=status&direction='.$newdir?>">Status</a></b></td>
	<td width="150"><b><a href="<?echo $base.'stulist.php?orderby=Signup_Date&direction='.$newdir?>">Signup_Date</a></b></td>
	<td width="100"><b><a href="<?echo $base.'stulist.php?orderby=visits&direction='.$newdir?>">Total logins</a></b></td>
    <td width="100"><b><a href="<?echo $base.'stulist.php?orderby=sumpg&direction='.$newdir?>">Pages Taken till now</a></b></td>
	<td width="100"><b><a href="<?echo $base.'stulist.php?orderby=logged&direction='.$newdir?>">Logged Status</a></b></td>
	<td width="130" colspan="2"><p align="center"><b>Action</b></td>
  </tr>

  <?
  $sorting="";
  if($orderby!="" && $direction!="")  $sorting=" ORDER BY $orderby $direction";
    
  $query2 = "SELECT stu_list.Student_No, Name, Gender, Course, Branch, Batch, Phone_Number, Email, sumpg, visits, Permitted, status, Signup_Date, logged
FROM stu_list
LEFT JOIN (

SELECT SUM( Pg_Requested ) sumpg, Student_No
FROM stu_stationary
WHERE `approval` LIKE 'approved'
GROUP BY Student_No
)sta ON stu_list.Student_No = sta.Student_No
LEFT JOIN (

SELECT COUNT( Student_No ) visits, Student_No
FROM stu_entry
GROUP BY Student_No
)entry ON stu_list.Student_No = entry.Student_No
$sorting
";
  
  
  
  
  
  $result2 = mysql_query($query2);
  ?>
  <tr>
  <?
  $i=0;
  while ($row2 = mysql_fetch_array($result2)) 
  {
  $i++;
  ?>
  <td><?echo $i?></td>
  <td><?echo $row2['Name']?></td>
  <td><?echo $row2['Student_No']?></td>
  <td><?echo $row2['Gender']?></td>
  <td><?echo $row2['Course']?></td>
  <td><?echo $row2['Branch']?></td>
  <td><?echo $row2['Batch']?></td>
  <td><?echo $row2['Phone_Number']?></td>
  <td><?echo $row2['Email']?></td>  
  <td><?echo $row2['Permitted']==1?'allowed':'banned'?></td>  
  <td><?echo $row2['status']?></td>
  <td><?echo date('F d, Y \a\t h:i:s a',strtotime($row2['Signup_Date']))?></td>
  <td><?echo $row2['visits']?></td>
  <td><?echo $row2['sumpg']?></td>
  <td><?echo $row2['logged']?></td>
  <td><?echo "<input name='approve' value='approve' type='button' onClick=\"if(confirm('are you sure you wish to approve \'".$row2['Pg_Requested']."\' pages request by \'".$row2['Name']."\' with student number \'".$row2['Student_No']."\' ? PRESS OK ONLY AFTER YOU GIVE PAGES TO STUDENT.')) {document.getElementById('stno').value='".$row2['id']."';document.getElementById('app').value='approve';document.getElementById('stulist').submit();}\"/>";
?></td>
  <td><?echo "<input name='dismiss' value='dismiss' type='button' onClick=\"if(confirm('are you sure you wish to dismiss \'".$row2['Pg_Requested']."\'pages requested by \'".$row2['Name']."\' with student number \'".$row2['Student_No']."\' ? ')) {document.getElementById('stno').value='".$row2['id']."';document.getElementById('app').value='dismiss';document.getElementById('stulist').submit();}\"/>";
?></td>
  
  </tr>
  
<?


	






} //end of while
?>
</table>
</form>
<br>
<input type="button" value="Print Report" name="Home" onClick="PrintContent()">

<?
} //end of if


?>
</div>

</body>
</html>


