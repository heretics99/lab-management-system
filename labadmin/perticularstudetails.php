<?
include("checksession.php");
include("dbconnect.php");
connect();
include("config.php");

$stno=$_GET['stno'];
$orderby=$_GET['orderby'];	
$direction=$_GET['direction'];
$newdir="ASC";
if($direction=='asc'||$direction=='ASC') {$direction="ASC"; $newdir="DESC";}
if($direction=='desc'||$direction=='DESC') {$direction="DESC"; $newdir="ASC";}

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Student Details for Student Number: <?echo $stno?></title>
<link rel="stylesheet" type="text/css" href="style.css"> 

<style type="text/css">
	#windowContent{	/* Normal text content */
		float:left;	/* Firefox - to avoid blank white space above panel */
		padding-left:10px;	/* A little space at the left */
	}	
</style>
<script language="javascript" src="printfunction.js"></script>
</head>

<body>

<?php include("left.php");?> 
<div id="windowContent">

<p align="center"><b><span style="font-size: 20pt">Perticular Student Details</span></b></p>

<?

$query = "SELECT * FROM stu_list";
$result = mysql_query($query);
$row = mysql_fetch_array($result);

if(!$row) echo "no students in database";

else
{
?>
<fieldset>
<legend>Student Details for Student Number: <?echo $stno?></legend>
<form name="studetails" id="studetails" action="studetailsmsg.php" method="POST">
<input type='hidden' name='stno' id='stno'>
<input type='hidden' name='app' id='app'>


<table border="2" cellpadding="5" cellspacing="1" style="border-collapse: collapse" bordercolor="#999999" width="100%">
  
  <?
  $sorting="";
  if($orderby!="" && $direction!="")  $sorting=" ORDER BY $orderby $direction";
    
$query2 = "SELECT *,stu_list.Student_No, sumpg, visits
FROM stu_list
LEFT JOIN 
(
SELECT SUM( Pg_Requested ) sumpg, Student_No
FROM stu_stationary
WHERE `approval` LIKE 'approved'
GROUP BY Student_No
)sta 
ON stu_list.Student_No = sta.Student_No
LEFT JOIN 
(
SELECT COUNT( Student_No ) visits, Student_No
FROM stu_entry
GROUP BY Student_No
)entry 
ON stu_list.Student_No = entry.Student_No WHERE stu_list.Student_No LIKE '$stno'
$sorting
";
  
  
  
  
  
  $result2 = mysql_query($query2);
  ?>
  
  <?
  $i=0;
  while ($row2 = mysql_fetch_array($result2)) 
  {
  $i++;
  ?>
  
<table border="0" cellpadding="2" cellspacing="0" style="border-collapse: collapse; border-width: 0" bordercolor="#111111" width="100%">
<u>Student Information</u>
  <tr>
    <td width="20%"><b>Name: </b></td>
    <td width="80%"><?echo $row2['Name']?></td>
  </tr>
  <tr>
    <td><b>Gender: </b></td>
    <td><?echo $row2['Gender']?></td>
  </tr>
  <tr>
    <td><b>Course: </b></td>
    <td><?echo $row2['Course']?></td>
  </tr>
  <tr>
    <td><b>Branch: </b></td>
    <td><?echo $row2['Branch']?></td>
  </tr>
  <tr>
    <td><b>Admission Year: </b></td>
    <td><?echo $row2['Addmission_Year']?></td>
  </tr>
  <tr>
    <td><b>Batch: </b></td>
    <td><?echo $row2['Batch']?></td>
  </tr>
  <tr>
    <td><b>Phone Number: </b></td>
    <td><?echo $row2['Phone_Number']?></td>
  </tr>
  <tr>
    <td><b>Email: </b></td>
    <td><?echo $row2['Email']?></td>
  </tr>
  <tr>
    <td><b>Status: </b></td>
    <td><?echo $row2['status']?></td>
  </tr>
  <tr>
    <td><b>Signup Date: </b></td>
    <td><?echo date('d-F-Y, h:i:s a',strtotime($row2['Signup_Date']))?></td>
  </tr>
  <tr>
    <td><b>Approval Date: </b></td>
    <td><?echo date('d-F-Y, h:i:s a ',strtotime($row2['approval_date']))?></td>
  </tr>
  </table>
  <hr>
  <table border="0" cellpadding="2" cellspacing="0" style="border-collapse: collapse; border-width: 0" bordercolor="#111111" width="100%">
<u>Present Information</u>
  <tr>
    <td width="20%"><b>Permission: </b></td>
    <td width="80%"><?echo $row2['Permitted']=='1'?'allowed in lab':'banned in lab'?></td>
  </tr>
  <tr>
    <td><b>Total logins: </b></td>
    <td><?echo $row2['visits']==0?"0":$row2['visits']?></td>
  </tr>
  <tr>
    <td><b>Total pages taken: </b></td>
    <td><?echo $row2['sumpg']==0?"0":$row2['sumpg']?></td>
  </tr>
  </table>
  <hr>
  <style type="text/css">
	
.CollapsiblePanelContent10 { 
	#overflow: scroll;
	#height: 300px;
	font-size:80%;
	white-space:normal;
	
}
.CollapsiblePanel10 {
	width: 600px;
	font-size:120%;
}
.CollapsiblePanelTab10 {
	font-size: 1em;
	
}
</style>
<div id="CollapsiblePanelGroup10" class="CollapsiblePanelGroup">
	<div class="CollapsiblePanel10">
		<div class="CollapsiblePanelTab" tabindex="0">Ban Reports</div>
		<div class="CollapsiblePanelContent10">
			<?
			$query = "SELECT * FROM stu_ban WHERE Student_No LIKE '$stno' ORDER BY ban_apply DESC";
			$result = mysql_query($query);
			
			if(!mysql_fetch_array($result)) print "Not banned ever";
			
			$result = mysql_query($query);
			$i=0;
			
			while ($row = mysql_fetch_array($result)) 
			{
				$i++;
			
			
			?>
			<table border="0" cellpadding="2" cellspacing="0" style="border-collapse: collapse; border-width: 0" bordercolor="#111111" width="100%">
			<p><b>----Ban#<?echo $i?>----</b></p>
			<tr>
				<td><b>Ban Apply Time: </b></td>
				<td><?echo date('d-F-Y, h:i:s a',strtotime($row['ban_apply']))?></td>
			</tr>			
			<tr>
				<td width="20%"><b>Reason: </b></td>
				<td width="80%"><?echo $row['reason']?></td>
			</tr>
			<tr>
				<td><b>Ban Lift Time: </b></td>
				<td><?echo $row['ban_lift']=="0000-00-00 00:00:00"?"- (Still active)":date('d-F-Y, h:i:s a',strtotime($row['ban_lift']))?></td>
			</tr>
			</table>
			<hr>
			<?
			}
			?>
			
			
			
		</div>
	</div>

</div>

   
  
<?

} //end of while
?>
</table>
</form>
<br>
<input type="button" value="Print Report" name="Home" onClick="PrintContent()">

<?
} //end of if


?>
</div>

<script language="JavaScript" type="text/javascript">
var cpg10 = new Spry.Widget.CollapsiblePanelGroup("CollapsiblePanelGroup10", { contentIsOpen: false });
</script>

</body>
</html>


