<?
include("checksession.php");
include("dbconnect.php");
connect();
include("config.php");

$comp=$_GET['comp'];

$orderby=$_GET['orderby'];	
$direction=$_GET['direction'];
$newdir="ASC";
if($direction=='asc'||$direction=='ASC') {$direction="ASC"; $newdir="DESC";}
if($direction=='desc'||$direction=='DESC') {$direction="DESC"; $newdir="ASC";}


?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>History for Computer No: <?echo $comp?></title>
<link rel="stylesheet" type="text/css" href="style.css"> 
<script language="javascript" src="printfunction.js"></script>
</head>

<body>
<?php include("left.php");?> 
<div id="windowContent">

<p align="center"><b><span style="font-size: 20pt">Computer History</span></b></p>

<p><span style="font-size: 13pt"><b><u>History for Computer No: <?echo $comp?></u></b></span></p>

<table border="2" cellpadding="5" cellspacing="1" style="border-collapse: collapse" bordercolor="#999999" width="100%">
  <tr>
	<td width="10"><b>Sno.</b></td>
	<td width="10"><b><a  title="sort Student No by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'perticularcompdetails.php?comp='.$comp.'&orderby=Student_No&direction='.$newdir?>">Used by Student No.</a></b></td>
	<td width="10"><b><a  title="sort In Time by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'perticularcompdetails.php?comp='.$comp.'&orderby=In_Time&direction='.$newdir?>"> In Time</a></b></td>
	<td width="150"><b><a  title="sort Out Time by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'perticularcompdetails.php?comp='.$comp.'&orderby=Out_Time&direction='.$newdir?>"> Out Time</a></b></td>
	<td width="150"><b><a title="sort Elapsed Time by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'perticularcompdetails.php?orderby=elapsed&direction='.$newdir?>">For Time duration</a></b></td>
	
  </tr>

  <?
  $sorting="";
  if($orderby!="" && $direction!="")  $sorting=" ORDER BY $orderby $direction";
    
$query = "SELECT Student_No,In_Time,Out_Time,(unix_timestamp(Out_Time) - unix_timestamp(In_Time)) elapsed FROM `stu_entry` WHERE Computer_No='".$comp."' $sorting";
$result = mysql_query($query);
  ?>
  <tr>
  <?
  $i=0;
  while ($row = mysql_fetch_array($result)) 
  {
  $i++;
  $elapsed=$row['elapsed'];
   if($elapsed<0) $elapsed=time()-strtotime($row['In_Time']);
  
  //echo $elapsed."<br>";
   $days=floor($elapsed / 84600);
   $elapsed -= 84600 * floor($elapsed / 84600);

   $hours=floor($elapsed / 3600);
   $elapsed -= 3600 * floor($elapsed / 3600);
  // echo $hours;
   $minutes = floor($elapsed / 60);
   $elapsed -= 60 * floor($elapsed / 60);
   
   $seconds=$elapsed;
   //echo ' minutes, and'. $elapsed.' seconds ago.';
  ?>
  <td> <?echo $i?></td>
  <td><a title='view student details' href='perticularstudetails.php?stno=<?echo $row['Student_No']?>'><?echo $row['Student_No']?></a></td>
  <td> <?echo date('h:i:sa, d-m-y',strtotime($row['In_Time']))?></td>
  <td> <?echo $row['Out_Time']=="0000-00-00 00:00:00"?"-":date('h:i:sa, d-m-y',strtotime($row['Out_Time']))?></td>
  <td><?
  if($days>0) $timeelap= "$days days, $hours hours, $minutes minutes, and $seconds seconds";
  elseif($hours>0) $timeelap= "$hours hours, $minutes minutes, and $seconds seconds";
  elseif($minutes>0) $timeelap= "$minutes minutes, and $seconds seconds";
  elseif($seconds>=0) $timeelap= "$seconds seconds";?>
  <?echo $timeelap;?>
  </td>
  
  </tr>
  
<?

} //end of while
?>
</table>
<br>
<input type="button" value="Print Report" name="Home" onClick="PrintContent()">

</div>

</body>
</html>


