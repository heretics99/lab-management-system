<?
include("checksession.php");
include("dbconnect.php");
connect();
include("config.php");

$orderby=$_GET['orderby'];	
$direction=$_GET['direction'];
$newdir="ASC";
if($direction=='asc'||$direction=='ASC') {$direction="ASC"; $newdir="DESC";}
if($direction=='desc'||$direction=='DESC') {$direction="DESC"; $newdir="ASC";}

?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Active systems in use</title>
<link rel="stylesheet" type="text/css" href="style.css"> 
<script language="javascript" src="printfunction.js"></script>
</head>

<body>
<?php include("left.php");?> 
<div id="windowContent">

<p align="center"><b><span style="font-size: 20pt">Active systems</span></b></p>

<?

$query = "SELECT * FROM lab_active LEFT JOIN (SELECT Student_No,In_Time FROM `stu_entry` WHERE `stu_entry`.`Out_Time` = '0000-00-00 00:00:00') intime ON `intime`.`Student_No`=`lab_active`.`laststno`";
$result = mysql_query($query);
$row = mysql_fetch_array($result);

if(!$row) echo "no computer in the active lab available. Please choose another active lab with at least 1 computer.";

else
{
$query6="SELECT * FROM `lab_active` WHERE `status` LIKE 'inuse'";
$result6 = mysql_query($query6);
$inuse = mysql_num_rows($result6);

$query7="SELECT * FROM `lab_active` WHERE `status` LIKE 'free'";
$result7 = mysql_query($query7);
$free = mysql_num_rows($result7);

$query8="SELECT * FROM `lab_active` WHERE `status` LIKE 'free, disabled '";
$result8 = mysql_query($query8);
$disabled = mysql_num_rows($result8);

$query8="SELECT * FROM `lab_active`";
$result8 = mysql_query($query8);
$totalsys = mysql_num_rows($result8);
?>
<p><span style="font-size: 13pt"><b><u>Active systems in use</u></b></span></p>
Systems in use: <b><?echo $inuse?></b><br>
Systems free: <?echo $free?><br>
Systems disabled: <?echo $disabled?><br>
Total systems:<?echo $totalsys?><br>
<?
$query2 = "SELECT * FROM `lab_info` WHERE Lab_ID='".$row['Lab_ID']."'";
$result2 = mysql_query($query2);
$row2 = mysql_fetch_array($result2);

echo "<h4>The active lab is : <u>".$row2['Lab_Name']."</u> (Lab Id:".$row['Lab_ID'].")</h4>";
?>

<form name="activesystems" id="activesystems" action="activesystemsmsg.php" method="POST">
<input type='hidden' name='stno' id='stno'>
<input type='hidden' name='app' id='app'>
<input type='hidden' name='Lab_ID' id='Lab_ID'>
<input type='hidden' name='Computer_No' id='Computer_No'>
<table border="2" cellpadding="5" cellspacing="1" style="border-collapse: collapse" bordercolor="#999999" width="100%">
  <tr>
	<td width="10"><b>Sno.</b></td>
	<td width="10"><b><a  title="sort Computer No. by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'activesystems.php?orderby=Computer_No&direction='.$newdir?>"> Computer No.</a></b></td>
	<td width="150"><b><a  title="sort Computer Confi. by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'activesystems.php?orderby=compconfi&direction='.$newdir?>"> Computer Confi.</a></b></td>
    <td width="120"><b><a title="sort Status by <?echo $newdir=='ASC'?'ascending':'descending'?> order"  href="<?echo $base.'activesystems.php?orderby=status&direction='.$newdir?>">Status</a></b></td>
    <td width="10"><b><a title="sort Condition by <?echo $newdir=='ASC'?'ascending':'descending'?> order"  href="<?echo $base.'activesystems.php?orderby=`condition`&direction='.$newdir?>">Condition</a></b></td>
	<td width="10"><b><a  title="sort Last used Student No by <?echo $newdir=='ASC'?'ascending':'descending'?> order" href="<?echo $base.'activesystems.php?orderby=laststno&direction='.$newdir?>">Last used by Student No.</a></b></td>
	<td width="120" ><p align="center"><b>Action</b></td>
  </tr>

  <?
  $sorting="";
  if($orderby!="" && $direction!="")  $sorting=" ORDER BY $orderby $direction";
    
  $query2 = "SELECT * FROM lab_active LEFT JOIN (SELECT Student_No,In_Time FROM `stu_entry` WHERE `stu_entry`.`Out_Time` = '0000-00-00 00:00:00') intime ON `intime`.`Student_No`=`lab_active`.`laststno` $sorting";
  //	print $query2;
  $result2 = mysql_query($query2);
  ?>
  <tr>
  <?
  $i=0;
  while ($row2 = mysql_fetch_array($result2)) 
  {
  $i++;
  ?>
  <td <?if($row2['condition']=="disabled") echo " bgcolor='#D6D6D6'"?>><?echo $i?></td>
  <td <?if($row2['condition']=="disabled") echo " bgcolor='#D6D6D6'"?>><a title='view computer details' href='perticularcompdetails.php?comp=<?echo $row2['Computer_No']?>&orderby=In_Time&direction=DESC'><?echo "<b>".$row2['Computer_No']."</b>"?></a></td>
  <td <?if($row2['condition']=="disabled") echo " bgcolor='#D6D6D6'"?>><?echo $row2['compconfi']?></td>
  <td <?if($row2['condition']=="disabled") echo " bgcolor='#D6D6D6'"?>><?echo $row2['status']; if($row2['status']=='inuse') echo " (since ".date('h:i:sa, d-m-y',strtotime($row2['In_Time'])).")"?></td>
  <td <?if($row2['condition']=="disabled") echo " bgcolor='#D6D6D6'"?>><?echo $row2['condition']=='enabled'?"enabled":"DISABLED"?></td>
  <td <?if($row2['condition']=="disabled") echo " bgcolor='#D6D6D6'"?>><a title='view student details' href='perticularstudetails.php?stno=<?echo $row2['laststno']?>'><?echo $row2['laststno']?></a></td>
  <td <?if($row2['condition']=="disabled") echo " bgcolor='#D6D6D6'"?>><p align="center">
  <?
  if($row2['status']=='inuse')
  echo "<input title='logout user + disable' name='logout user' value='logout user + disable' type='button' onClick=\"if(confirm('Press OK if you wish to remotely logout student no. \'".$row2['laststno']."\' AND disable computer No. \'".$row2['Computer_No']."\'.')) {document.getElementById('Lab_ID').value='".$row2['Lab_ID']."';document.getElementById('Computer_No').value='".$row2['Computer_No']."';document.getElementById('stno').value='".$row2['laststno']."';document.getElementById('app').value='logout';document.getElementById('activesystems').submit();}\"/>";
  else if($row2['condition']=='enabled')
  echo "<input name='disable' value='disable' title='Disable computer' type='button' onClick=\"if(confirm('Press OK if you wish to disable computer no. \'".$row2['Computer_No']."\'. This computer will not be automatically alloted to any student if its in disabled state.')) {document.getElementById('Lab_ID').value='".$row2['Lab_ID']."';document.getElementById('Computer_No').value='".$row2['Computer_No']."';document.getElementById('app').value='disable';document.getElementById('activesystems').submit();}\"/>";
  else if($row2['condition']=='disabled')
  echo "<input name='enable' value='enable' type='button' onClick=\"if(confirm('Press OK if you wish to enable computer no. \'".$row2['Computer_No']."\'. ')) {document.getElementById('Lab_ID').value='".$row2['Lab_ID']."';document.getElementById('Computer_No').value='".$row2['Computer_No']."';document.getElementById('app').value='enable';document.getElementById('activesystems').submit();}\"/>";
?></td>
  
  </tr>
  
<?


	






} //end of while
?>
</table>
</form>
<br>
<input type="button" value="Print Report" name="Home" onClick="PrintContent()">

<?
} //end of if


?>
</div>

</body>
</html>


