<?
include("checksession.php");
include("dbconnect.php");
connect();
include("config.php");

$orderby=$_GET['orderby'];	
$direction=$_GET['direction'];
$newdir="ASC";
if($direction=='asc'||$direction=='ASC') {$direction="ASC"; $newdir="DESC";}
if($direction=='desc'||$direction=='DESC') {$direction="DESC"; $newdir="ASC";}

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Print Reports</title>
<link rel="stylesheet" type="text/css" href="style.css"> 
<script language="javascript" src="checkbox.js"></script>
<script language="javascript" src="printfunction.js"></script>
</head>

<body>
<?php include("left.php");?> 
<div id="windowContent">

<p align="center"><b><span style="font-size: 20pt">Print Reports</span></b></p>
<p><span style="font-size: 13pt"><b><u>Print Reports</u></b></span></p>
<p>Print Student entries between a pair of dates</p>

<link type="text/css" href="datepicker/css/ui-lightness/jquery-ui-1.8.11.custom.css" rel="stylesheet" />
<script type="text/javascript" src="datepicker/js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="datepicker/js/jquery-ui-1.8.11.custom.min.js"></script>
<script>
$(function()
{
        $( "#startdate" ).datepicker();
        $( "#enddate" ).datepicker();
});
</script>


<form name="printreports" id="printreports" action="printreportsmsg.php" method="POST">
<input type='hidden' name='stno' id='stno'>
<input type='hidden' name='app' id='app'>
<p>Starting Date: <input id="startdate" name="startdate" type="text"></p>
<p>Ending Date: <input id="enddate" name="enddate" type="text"></p>

<br>

<input type="submit" name="go" value="Go">
<br>


<br>
</form>


</div>

</body>
</html>


