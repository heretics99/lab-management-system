<?
include("checksession.php");
include("dbconnect.php");
connect();
include("config.php");

$lab_id=$_GET['LabID'];
$orderby=$_GET['orderby'];	
$direction=$_GET['direction'];
$newdir="ASC";
if($direction=='asc'||$direction=='ASC') {$direction="ASC"; $newdir="DESC";}
if($direction=='desc'||$direction=='DESC') {$direction="DESC"; $newdir="ASC";}

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>LAB Details</title>
<link rel="stylesheet" type="text/css" href="style.css"> 
<script language="javascript" src="printfunction.js"></script>
</head>

<body>
<?php include("left.php");?> 
<div id="windowContent">

<p align="center"><b><span style="font-size: 20pt">LAB Details</span></b></p>

<?
if(isset($lab_id))
{ // begin if isset($lab_id)

$query = "SELECT * FROM lab_info WHERE Lab_ID = '$lab_id'";
$result = mysql_query($query);
$row = mysql_fetch_array($result);

if($lab_id==$row['Lab_ID'])
{
?>
<p><span style="font-size: 13pt"><b><u><?echo $row['Lab_Name']?></u></b></span></p>
<table border="2" cellpadding="10" cellspacing="1" style="border-collapse: collapse" bordercolor="#999999" width="100%">
  <tr>
    <td width="30"><b><a href="<?echo $base.'lab_details.php?LabID='.$lab_id.'&orderby=Lab_ID&direction='.$newdir?>">Lab ID</a></b></td>
    <td width="30"><b><a href="<?echo $base.'lab_details.php?LabID='.$lab_id.'&orderby=No_Comp&direction='.$newdir?>">No of Computers</a></b></td>
    <td width="200"><b><a href="<?echo $base.'lab_details.php?LabID='.$lab_id.'&orderby=Comp_Config&direction='.$newdir?>">Computer Configuration</a></b></td>
    <td width="40"><b><a href="<?echo $base.'lab_details.php?LabID='.$lab_id.'&orderby=Comp_Name&direction='.$newdir?>">Computer Name</a></b></td>
    <td width="130"><b><a href="<?echo $base.'lab_details.php?LabID='.$lab_id.'&orderby=Date_Purchase&direction='.$newdir?>">Date of Purchase</a></b></td>
  </tr>

  <?
  $sorting="";
  if($orderby!="" && $direction!="")  $sorting=" ORDER BY $orderby $direction";
  $query2 = "SELECT * FROM `comp_info` WHERE `Lab_ID` =$lab_id $sorting";
  $result2 = mysql_query($query2);
  ?>
  <tr>
  <?
  while ($row2 = mysql_fetch_array($result2)) 
  {
  ?>
  <td width="20"><?echo $row2['Lab_ID']?></td>
  <td width="20"><?echo $row2['No_Comp']?></td>
  <td width="20"><?echo $row2['Comp_Config']?></td>
  <td width="20"><?echo $row2['Comp_Name']?></td>
  <td width="20"><?echo date('F d, Y',strtotime($row2['Date_Purchase']))  ?></td>
  </tr>
  
<?
} //end of while
?>
</table>
<br>
<input type="button" value="Print Report" name="Home" onClick="PrintContent()">
<input type="button" value="Choose another lab" name="Home" onClick="location.href='<?echo $base."lab_details.php"?>'">
<?
} //end of if
else print "No such lab exist";


}// end of if isset($lab_id)

else
{ //begin of if NOT isset($lab_id)
?>
<p><span style="font-size: 13pt"><b>Choose a lab:</b></span></p>

<?
$query = "SELECT * FROM lab_info ORDER BY Lab_Name ASC";
$result = mysql_query($query);
?>
<ol>
<?
while ($row = mysql_fetch_array($result)) 

{

?>
<li><p align="left"><span style="font-size: 13pt"><a href="<?echo $base."lab_details.php?LabID=".$row['Lab_ID']?>"><? echo $row['Lab_Name'];?></a></li>

<?
}// end of while
?>
</ol>
<br>
<br>
<?
} //end of if NOT isset($lab_id)
?>
<p><span style="font-size: 13pt"><b><a href="add_computer.php">Add a computer</a></b></span></p>
<b><a href="<?echo $base."adminop.php"?>"> Admin HOME</a></b>
</div>

</body>
</html>


