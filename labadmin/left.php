<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome</title>
<script language="javascript" src="left.js"></script>
<style type="text/css">
<!--
.std {font-size:small;font-family:arial,sans-serif}
li.g {font-size:small;font-family:arial,sans-serif}
#mbEnd {float:left}
#mbEnd {background:#fff;padding:0;border-left:1px solid #fff;border-spacing:0;white-space:nowrap}
-->
</style>
<style type="text/css">
	
#cp1 .CollapsiblePanelContent { 
	overflow: scroll;
	height: 300px;
	white-space:normal;
	
	
}
.CollapsiblePanel {
	width: 200px;
	font-size:120%;
}
.CollapsiblePanelTab {
	font-size: 1em;
	
}
</style>
<link href="collapse1.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/form-submit.js"></script>
<script type="text/javascript" src="js/ajax.js"></script>
	
</head>

<body>
<table width="17%" id="mbEnd" style="margin-bottom: 1em;">
  <tbody>
    <tr>
    <td id="rhsline" style="border-right: 1px solid rgb(201, 215, 241); padding-right: 8px;" class="std"><h2 style="margin: 0pt; padding: 2px 0pt 0pt; text-align: center;">Admin Options</h2></br>

	<div align="center">
	Welcome <b><?echo $_SESSION['adminuser_logged_in'];?></b>!<br><br>
	<form action="index.php" method="POST">
	<input style="align:center" type="submit" value="logout" name="logout">
	 </form>
	 
	 
	 
	<div id="CollapsiblePanelGroup1" class="CollapsiblePanelGroup">
	<div class="CollapsiblePanel">
	<div class="CollapsiblePanelTab" tabindex="0">Change Password</div>
	<div class="CollapsiblePanelContent">
	
	<form name="changepassword" method="POST">
	<div id="changepassworddiv" style="font-size:12px;white-space:normal">

	
		<table style="font-size:12px">
		<tr>
			<td>
			Current Password:</td>
		</tr>
		<tr>
			<td>
			<input type="password" name="currentpass" id="currentpass" maxlength="50"></td>
		</tr>
		<tr>
			<td>
			New Password:</td>
		</tr>
		<tr>
			<td>
			<input type="password" name="pass11" id="pass11" maxlength="50"></td>
		</tr>
		<tr>
			<td>
			Retype New Password:</td>
		</tr>
		<tr>
			<td>
			<input type="password" name="pass22" id="pass22" maxlength="50"> <p><i></i>
			</td>
		</tr>  
		<tr>
			<td>
			<input type="button" id="mySubmit" value="Submit" onclick='if(pass11.value==pass22.value) formObj1.submit(); else {alert("both passwords should match");pass22.focus()}'>
			</td>
		</tr>
		</table>
		<br>

	</div>
	</form>
	
	<script type="text/javascript">
var formObj1 = new dhtmlform.form({ formRef:'changepassword',action:'changepassword.php',responseEl:'changepassworddiv'});
</script>

	</div>
	</div>
	</div>
	</div>	  

<div align="center">
			
<?if(isset($_SESSION['super_is_logged_in'])) {?>
			

	<div id="CollapsiblePanelGroup2" class="CollapsiblePanelGroup">
	<div class="CollapsiblePanel">
	<div class="CollapsiblePanelTab" tabindex="0">Add new Administrator</div>
	<div class="CollapsiblePanelContent">
	<script language="javascript" src="addnewadmin.js"></script>	
	<form name="newadmin" method="POST">
	<div id="newadmindiv" style="font-size:12px;white-space:normal">

	<input type="hidden" name="flag" value="newadmin">	
	<table style="font-size:12px">
  <tr>
    <td>
    Username:</td>
	</tr>
	<tr>
    <td>
    <input type="text" name="username" id="username" size="20" maxlength="100">
    </td>
  </tr>
  <tr>
	<td>
    Password:</td>
	</tr>
  <tr>
	<td>
	<input type="password" name="pass1" id="pass1" size="20" maxlength="100">
    </td>
  </tr>
  <tr>
    <td>
    Confirm Password:</td></tr>
  <tr>
	<td>
	<input type="password" name="pass2" id="pass2" size="20" maxlength="100">
    </td>
  </tr>
  <tr>
    <td>
    Permission level:</td>
	</tr>
  <tr>
	<td>
	<input type="text" name="permission" id="permission" size="20" maxlength="1" value="1">
    </td>
  </tr>
  <tr>
    <td>
    New Admin Name: </td></tr>
  <tr>
	<td>	
    <input type="text" name="name" id="name" size="20" maxlength="100">
    </td>
  </tr>
  <tr>
    <td>
    Employee Number:</td></tr>
  <tr>
	<td>	
	<input type="text" name="empno" id="empno" size="20" maxlength="100">
  </td>
  </tr>
  <tr>
    <td>
    Phone Number: </td>
    </tr>
  <tr>
	<td>
	
    <input type="text" name="pno" id="pno" size="20" maxlength="10">
    </td>
  </tr>
  <tr>
    <td>
    Email: </td>
    </tr>
  <tr>
	<td>	
    <input type="text" name="email" id="email" size="20" maxlength="100">
    </td>
  </tr>
  <tr>
	<td>
	<input type="button" id="mySubmit" value="Submit" onclick='if(formValidator()) formObj2.submit();'>
    </td>
  </tr>
</table>

		<br>

	</div>
	</form>
	
	<script type="text/javascript">
var formObj2 = new dhtmlform.form({ formRef:'newadmin',action:'addnewadmin.php',responseEl:'newadmindiv'});
</script>

	</div>
	</div>
	</div>
	</div>	  	
			  <?}
			  ?>
			  
			  </div>
			  </div>
			  
			  
          
			
              <h3>Recent Notifications</h3>
			
			  <ul>
			  <li style="margin-top: -15">
              <a href="newstu.php?orderby=Signup_Date&direction=DESC">New student registration <br/>requests<br /></a>
              </li>
			  <li>
              <a href="stationaryrequests.php?orderby=dateval&direction=DESC">Stationary Requests<br /></a>
              </li>
			  <li>
              <a href="forgotpassword.php?orderby=fpdate&direction=DESC">Password Reset Requests<br /></a>
              </li>
			  <li>
              <a href="compcomplaints.php?orderby=dateval&direction=DESC">Computer Complaints<br /></a>
              </li>
			  </ul>
			  
              <h3>Student Reports</h3>
			  
			  <ul>
			  <li style="margin-top: -15">
              <a href="studetails.php?orderby=Signup_Date&direction=DESC">All Student Details<br /></a>
              </li>
			  <li>
              <a href="stuactive.php?orderby=In_Time&direction=DESC">Active student logins</a><br />
              </li>
			  <li>
              <a href="stulogintoday.php?orderby=In_Time&direction=DESC">Student logins today<br /></a>
              </li>
                        <li>
              <a href="printreports.php">Print Reports<br /></a>
              </li>
			  <li>
              <a href="stuloginall.php?orderby=In_Time&direction=DESC">All Student Lab entries<br />
              </li>
			  <li>
              <a href="stuban.php">Ban a student<br /></a>
              </li>
			  <li>
              <a href="stupermit.php?orderby=ban_apply&direction=DESC">Unban a student<br /></a>
              </li>
			  <li>
              <a href="banreport.php?orderby=ban_apply&direction=DESC">Ban Reports<br /></a>
              </li>
			  </ul>
			  
              <h3>Stationary</h3>
			  
			  <ul>
			  <li style="margin-top: -15">
              <a href="stationaryrequests.php?orderby=dateval&direction=DESC">New requests<br /></a>
              </li>
			  </ul>
        
              <h3>Lab Status</h3>
			  
			  <ul>
			  <li style="margin-top: -15">
              <a href="activesystems.php?orderby=Computer_No&direction=ASC">Active systems in use<br /></a>
              </li>
			  <li>
              <a href="activelab.php">Choose active lab<br /></a>
              </li>
			  </ul>

			  
              
        </p></td>
		<td>
		
		</td>
    </tr>
    <tr>
	<td>
	
	</td>
      <td id="rhspad"></td>
    </tr>
  </tbody>
</table>
<script language="JavaScript" type="text/javascript">
var cpg1 = new Spry.Widget.CollapsiblePanelGroup("CollapsiblePanelGroup1", { contentIsOpen: false });
var cpg2 = new Spry.Widget.CollapsiblePanelGroup("CollapsiblePanelGroup2", { contentIsOpen: false });
var cpg3 = new Spry.Widget.CollapsiblePanelGroup("CollapsiblePanelGroup3", { contentIsOpen: false });
//var cpg2_ = new Spry.Widget.CollapsiblePanelGroup("CollapsiblePanelGroup2_", { contentIsOpen: false, openPanelKeyCode:34, closePanelKeyCode:33});
//var cpg3 = new Spry.Widget.CollapsiblePanelGroup("CollapsiblePanelGroup3", { contentIsOpen: false, enableAnimation: false });
//var cpg = new Spry.Widget.CollapsiblePanelGroup("CollapsiblePanelGroup5", { contentIsOpen: false });
</script>
</body>
</html>
