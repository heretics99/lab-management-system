-- phpMyAdmin SQL Dump
-- version 3.1.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 09, 2011 at 04:19 AM
-- Server version: 5.1.30
-- PHP Version: 5.2.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lab`
--
CREATE DATABASE `lab` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `lab`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `permission` int(1) NOT NULL,
  `empno` varchar(100) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `phone` int(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `datereg` datetime NOT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `username_2` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comp_complaint`
--

CREATE TABLE `comp_complaint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Student_No` varchar(8) NOT NULL,
  `Lab` varchar(100) NOT NULL,
  `Computer_No` varchar(6) NOT NULL,
  `Complaint` varchar(2000) NOT NULL,
  `dateval` datetime NOT NULL,
  `attenddateval` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1076 ;

-- --------------------------------------------------------

--
-- Table structure for table `comp_info`
--

CREATE TABLE `comp_info` (
  `Lab_ID` int(11) NOT NULL,
  `No_Comp` int(11) NOT NULL,
  `Comp_Config` varchar(200) NOT NULL,
  `Comp_Name` varchar(50) NOT NULL,
  `Date_Purchase` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `in_out`
--

CREATE TABLE `in_out` (
  `From_Lab` int(11) NOT NULL,
  `To_Lab` int(11) NOT NULL,
  `No_Comp` int(11) NOT NULL,
  `System_Srno` varchar(50) NOT NULL,
  `System_Config` varchar(200) NOT NULL,
  `Transfer_Date` datetime NOT NULL,
  `Given_LabAssist` varchar(100) NOT NULL,
  `Taken_LabAssist` varchar(100) NOT NULL,
  KEY `From_Lab` (`From_Lab`),
  KEY `To_Lab` (`To_Lab`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lab_active`
--

CREATE TABLE `lab_active` (
  `Lab_ID` int(11) NOT NULL,
  `Computer_No` int(10) NOT NULL,
  `compconfi` varchar(200) NOT NULL,
  `status` varchar(20) NOT NULL,
  `condition` varchar(20) NOT NULL,
  `laststno` varchar(9) NOT NULL,
  UNIQUE KEY `Computer_No` (`Computer_No`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lab_info`
--

CREATE TABLE `lab_info` (
  `Lab_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Lab_Name` varchar(200) NOT NULL,
  PRIMARY KEY (`Lab_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `prac_experiments`
--

CREATE TABLE `prac_experiments` (
  `paper_id` int(11) NOT NULL,
  `sub_code` varchar(7) NOT NULL,
  `sub_name` varchar(200) NOT NULL,
  `year` varchar(10) NOT NULL,
  `experiment` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stu_ban`
--

CREATE TABLE `stu_ban` (
  `banid` int(11) NOT NULL AUTO_INCREMENT,
  `Student_No` varchar(10) NOT NULL,
  `reason` varchar(500) NOT NULL,
  `ban_apply` datetime NOT NULL,
  `ban_lift` datetime NOT NULL,
  PRIMARY KEY (`banid`),
  FULLTEXT KEY `Student_No` (`Student_No`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=400 ;

-- --------------------------------------------------------

--
-- Table structure for table `stu_banreason`
--

CREATE TABLE `stu_banreason` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reason` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `stu_entry`
--

CREATE TABLE `stu_entry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Student_No` varchar(8) NOT NULL,
  `Lab_ID` int(11) NOT NULL,
  `Computer_No` varchar(6) NOT NULL,
  `In_Time` datetime NOT NULL,
  `Out_Time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `Student_No` (`Student_No`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7576 ;

-- --------------------------------------------------------

--
-- Table structure for table `stu_list`
--

CREATE TABLE `stu_list` (
  `Student_No` varchar(8) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Gender` varchar(10) NOT NULL,
  `Course` varchar(50) NOT NULL,
  `Branch` varchar(50) NOT NULL,
  `Addmission_Year` year(4) NOT NULL,
  `Batch` year(4) NOT NULL,
  `Phone_Number` varchar(10) NOT NULL,
  `Email` varchar(200) NOT NULL,
  `Permitted` tinyint(1) NOT NULL DEFAULT '1',
  `status` varchar(20) NOT NULL,
  `Signup_Date` datetime NOT NULL,
  `approval_date` datetime NOT NULL,
  `fp` tinyint(1) NOT NULL DEFAULT '0',
  `fpdate` datetime NOT NULL,
  `logged` varchar(20) NOT NULL,
  PRIMARY KEY (`Student_No`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stu_stationary`
--

CREATE TABLE `stu_stationary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Student_No` varchar(8) NOT NULL,
  `Pg_Requested` int(11) NOT NULL,
  `dateval` datetime NOT NULL,
  `approval_date` datetime NOT NULL,
  `approval` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Student_No` (`Student_No`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1286 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblusers`
--

CREATE TABLE `tblusers` (
  `iUser` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sEmail` varchar(255) NOT NULL,
  `sPassword` varchar(255) NOT NULL,
  `sGUID` varchar(32) DEFAULT NULL,
  `sData` text,
  PRIMARY KEY (`iUser`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
