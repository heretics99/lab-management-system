var diploma="no"
function findselected(){
var coursetext = document.getElementById('coursetext');

var w = document.newstudent.course.selectedIndex;
var selected_text = document.newstudent.course.options[w].text;

if(selected_text == "MCA" || selected_text=="MBA"){
document.getElementById('branch').style.display="none";
document.getElementById('coursetext').style.display="";
document.getElementById('coursetext').value=selected_text;
}
else
{
 document.getElementById('coursetext').style.display="none";
 document.getElementById('branch').style.display="";
}
 
}
//----------------------------------------------findselected() ends------------------

function fillValues()
{
	var a = detectCourseBranch(document.getElementById('stno'))

	document.getElementById('course').value=a.course;

	var b= detectAddYear(document.getElementById('stno'))
	document.getElementById('add_year').value=b;

	if(a.course=="PGDM" || a.course=="MCA")
	{
		document.getElementById("branchLabel").style.visibility = 'hidden';
		document.getElementById('branch').style.display="none";
		if(a.course=="PGDM")
		{
			document.getElementById('branch').value = 'PGDM';
		}
		else if(a.course=="MCA")
		{
			document.getElementById('branch').value = 'MCA';
		}

	}
	document.getElementById('branch').value=a.branch;
	//else if(a.course=="BTech") document.getElementById('branch').value=a.branch;
	//else if(a.course=="MTech") document.getElementById('branch').value=a.branch;
	//else alert("some problem")
	//alert(document.getElementById('add_year').value+a.course+diploma)
	populateBatchyear(document.getElementById('add_year').value,a.course)
}

//----------------------end of fillvalues()-----------------

function populateBatchyear(add_year,course) {
//year="2005";
yr=parseInt(add_year);


if(course=="BTech" && diploma=="yes")
{

for(var i=0; i<2; i++){
    document.newstudent.batch_year.options[i]=new Option(yr+3+i, yr+3+i);
    }
}
else if(course=="BTech" && diploma=="no")
{
for(var i=0; i<2; i++){
    document.newstudent.batch_year.options[i]=new Option(yr+4+i, yr+4+i);
    }
}
else if(course=="PGDM")
{
for(var i=0; i<2; i++){
    document.newstudent.batch_year.options[i]=new Option(yr+2+i, yr+2+i);
    }
}
else if(course=="MCA")
{
for(var i=0; i<2; i++){
    document.newstudent.batch_year.options[i]=new Option(yr+2+i, yr+2+i);
    }
}
else if(course=="MTech")
{
for(var i=0; i<2; i++){
    document.newstudent.batch_year.options[i]=new Option(yr+2+i, yr+2+i);
    }
}

}



function detectAddYear(stno)
{
	var yearshort=stno.value.substring(0,2)
	yearlong="20"+yearshort
	//alert(yearlong)
	return yearlong;
}

function detectCourseBranch(stno)
{
if((stno.value.length==7 ||stno.value.length==8) && stno.value.match(/^[01][0-9](10|13|21|31|40|32)[0-9]{3}D?$/))
{
	var re=/^[01][0-9](10|13|21|31|40|32)[0-9]{3}D?$/;
	if(stno.value.match(re))
	{
		course="BTech"
		if(stno.value.match(/^[01][0-9](10)[0-9]{3}$/)) branch="CSE"
		if(stno.value.match(/^[01][0-9](10)[0-9]{3}D$/)) {branch="CSE Diploma"; diploma="yes";}
		if(stno.value.match(/^[01][0-9](13)[0-9]{3}$/)) branch="IT"
		if(stno.value.match(/^[01][0-9](13)[0-9]{3}D$/)) {branch="IT Diploma"; diploma="yes";}
		if(stno.value.match(/^[01][0-9](21)[0-9]{3}$/)) branch="EN"
		if(stno.value.match(/^[01][0-9](21)[0-9]{3}D$/)) {branch="EN Diploma"; diploma="yes";}
		if(stno.value.match(/^[01][0-9](31)[0-9]{3}$/)) branch="EC"
		if(stno.value.match(/^[01][0-9](31)[0-9]{3}D$/)) {branch="EC Diploma"; diploma="yes";}
		if(stno.value.match(/^[01][0-9](40)[0-9]{3}$/)) branch="ME"		
		if(stno.value.match(/^[01][0-9](40)[0-9]{3}D$/)) {branch="ME Diploma"; diploma="yes";}
		if(stno.value.match(/^[01][0-9](32)[0-9]{3}$/)) branch="EI"		
		if(stno.value.match(/^[01][0-9](32)[0-9]{3}D$/)) {branch="EI Diploma"; diploma="yes";}
	}
	else
	{
		//alert("Invalid Student Number.")
		course="NA"
		branch="NA"
		diploma="no"
	}
}

else if((stno.value.length==7 ) && stno.value.match(/^[01][0-9](14)[0-9]{3}$/))
{
	var re=/^[01][0-9](14)[0-9]{3}$/;
	if(stno.value.match(re))
	{
		course="MCA"
		branch="MCA"
		diploma="no"
	}
	else
	{
		//alert("Invalid Student Number.")
		course="NA"
		branch="NA"
		diploma="no"
	}
}

else if(stno.value.length==5 ||stno.value.length==6)
{
	re=/^[01][0-9]{4}$/;
	if(stno.value.match(re))
	{
		course="PGDM"
		branch="NA"
		diploma="no"
	}
	else 
	{
		//alert("Invalid Student Number.")
		course="NA"
		branch="NA"
		diploma="no"
	}
}

else if((stno.value.length==7 ||stno.value.length==8) && (stno.value.match(/^[01][0-9](10|13|21|31|40|32)[0-9]{3}M$/)||stno.value.match(/^[01][0-9](98|25)[0-9]{3}$/)))
{
	var re1=/^[01][0-9](10|13|21|31|40|32)[0-9]{3}M$/;
	var re2=/^[01][0-9](98|25)[0-9]{3}$/;
	if(stno.value.match(re1)||stno.value.match(re2))
	{
		course="MTech"
		if(stno.value.match(/^[01][0-9](10)[0-9]{3}M$/)) branch="CSE MTech"
		else if(stno.value.match(/^[01][0-9](13)[0-9]{3}M$/)) branch="IT MTech"
		else if(stno.value.match(/^[01][0-9](21)[0-9]{3}M$/)) branch="EN MTech"
		else if(stno.value.match(/^[01][0-9](31)[0-9]{3}M$/)) branch="EC MTech"
		else if(stno.value.match(/^[01][0-9](40)[0-9]{3}M$/)) branch="ME MTech"	
		//exclusive MTech branches
		else if(stno.value.match(/^[01][0-9](98)[0-9]{3}$/)) branch="A&I MTech"
		else if(stno.value.match(/^[01][0-9](25)[0-9]{3}$/)) branch="EP&ES MTech"
		
	}
	else
	{
		//alert("Invalid Student Number.")
		course="NA"
		branch="NA"
		diploma="no"
	}
}



else
{
	//alert("Invalid Student Number.")
	course="NA"
	branch="NA"
	diploma="no"
}

return {course:course, branch:branch};
}


function formValidator(){
//alert(document.getElementById('batch_year').options[document.getElementById('batch_year').selectedIndex].value);
//return false
	// Make quick references to our fields
	var stno = document.getElementById('stno');
	var pass1 = document.getElementById('pass1');
	var pass2 = document.getElementById('pass2');
	var name = document.getElementById('name');
	var gender = document.getElementById('gender');
	var course = document.getElementById('course');
	var branch = document.getElementById('branch');
	var pno=document.getElementById('pno');
	var email=document.getElementById('email');
	
	// Check each input in the order that it appears in the form!
	if(isAlphanumeric(stno, "Please enter only alphanumeric characters for Student Number")){
		if(lengthRestriction(pass1,"password", 4, 50)){
			if(pass1.value==pass2.value){
				if(isAlphabet(name, "Please enter only letters for your name")){
					if(isNumeric(pno, "Please enter a valid 10 digit phone number")){
						if(lengthRestriction(pno, "phone number",10,13)){
						if(emailValidator(email, "Please enter a valid email address")){
							return true;
						}
						}
					}
				}
				
		}
		else {
		alert("both the passwords should match")
		document.getElementById('pass2').focus()
		}
		}
	}
				
		return false;	
	
}

function notEmpty(elem, helperMsg){
	if(elem.value.length == 0){
		alert(helperMsg);
		elem.focus(); // set the focus to this input
		return false;
	}
	return true;
}

function isNumeric(elem, helperMsg){
	var numericExpression = /^[0-9]+$/;
	if(elem.value.match(numericExpression)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-zA-Z\s]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphanumeric(elem, helperMsg){
	var alphaExp = /^[0-9a-zA-Z]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function lengthRestriction(elem,name, min, max){
	var uInput = elem.value;
	if(uInput.length >= min && uInput.length <= max){
		return true;
	}else{
		alert("Please enter "+name+" between " +min+ " and " +max+ " characters");
		elem.focus();
		return false;
	}
}

function madeSelection(elem, helperMsg){
	if(elem.value == "--Please select a lab--"){
		alert(helperMsg);
		elem.focus();
		return false;
	}else{
		return true;
	}
}

function emailValidator(elem, helperMsg){
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(elem.value.match(emailExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}




function showHide(){
//alert("clicked label")
document.getElementById("toggle").style.visibility = 'hidden';
document.getElementById('branch').style.display="none";
}
