<?
include("dbconnect.php");
connect();
include("config.php");

?>

<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Students Entry</title>
<script>

function formValidator(){

//return false
	// Make quick references to our fields
	var stno = document.getElementById('st_no');
	
	// Check each input in the order that it appears in the form!
	if(isStudentNo(stno)){
	return true;
	}
		
		
		return false;	
	
}

function isStudentNo(elem){
	var len=elem.value.length
	if(len==5)
	{
		var stnoPGDMExp = /^[01][0-9]{4}$/;
		if(elem.value.match(stnoPGDMExp)) 
		{
			return true;
		}
		else 
		{
			alert("Not a valid student number. "+'\n\n'+" If you are a PGDM student, example of valid student Number is: '08101'"+'\n\n'+" If you are a BTech or MCA student, some examples the valid student Numbers are:"+'\n'+""+'\n'+" '0910034' 	for CSE"+'\n'+" '1113056' 	for IT"+'\n'+" '0631034D' 	for EC Diploma"+'\n'+" '0721117' 	for EN"+'\n'+" '0740130D' 	for ME Diploma"+'\n\n'+" '0814056' 	for MCA"+'\n\n'+" Please add a trailing 'D' to your student number if you are diploma student. Example: 0810056D for CSE Diploma students"+'\n\n'+"*Note: If you are trying to enter a valid St. No and still being denied, please CONTACT the LAB ADMIN");
			return false;
		}
	}
/*	
	else if(len==6)
	{
		var stnoPGDMDipExp = /^[01][0-9]{4}D$/;
		if(elem.value.match(stnoPGDMDipExp))
		{
			return true;
		}
		else
		{
			alert("Not a valid student number. "+'\n\n'+" If you are a PGDM student, example of valid student Number is: '08101'"+'\n\n'+" If you are a BTech or MCA student, some examples the valid student Numbers are:"+'\n'+""+'\n'+" '0910034' 	for CSE"+'\n'+" '1113056' 	for IT"+'\n'+" '0631034D' 	for EC Diploma"+'\n'+" '0721117' 	for EN"+'\n'+" '0740130D' 	for ME Diploma"+'\n\n'+" '0814056' 	for MCA"+'\n\n'+" Please add a trailing 'D' to your student number if you are diploma student. Example: 0810056D for CSE Diploma students");
			return false;
		}
		
	}
	*/
	else if(len==7)
	{
		var stnoBTECHExp = /^[01][0-9](10|13|21|31|40|14)[0-9]{3}$/;
		if(elem.value.match(stnoBTECHExp))
		{
			return true;
		}
		else
		{
			alert("Not a valid student number. "+'\n\n'+" If you are a BTech or MCA student, some examples the valid student Numbers are:"+'\n'+""+'\n'+" '0910034' 	for CSE"+'\n'+" '1113056' 	for IT"+'\n'+" '0631034D' 	for EC Diploma"+'\n'+" '0721117' 	for EN"+'\n'+" '0740130D' 	for ME Diploma"+'\n\n'+" '0814056' 	for MCA"+'\n\n'+" If you are a PGDM student, example of valid student Number is: '08101'"+'\n\n'+" Please add a trailing 'D' to your student number if you are diploma student. Example: 0810056D for CSE Diploma students"+'\n\n'+"*Note: If you are trying to enter a valid St. No and still being denied, please CONTACT the LAB ADMIN");
			return false;
		}
	}
	
	else if(len==8)
	{
		var stnoBTECHDipExp = /^[01][0-9](10|13|21|31|40)[0-9]{3}D$/;
		if(elem.value.match(stnoBTECHDipExp)) 
		{
			return true;
		}
		else
		{
			alert("Not a valid student number. "+'\n\n'+" If you are a BTech or MCA student, some examples the valid student Numbers are:"+'\n'+""+'\n'+" '0910034' 	for CSE"+'\n'+" '1113056' 	for IT"+'\n'+" '0631034D' 	for EC Diploma"+'\n'+" '0721117' 	for EN"+'\n'+" '0740130D' 	for ME Diploma"+'\n\n'+" '0814056' 	for MCA"+'\n\n'+" If you are a PGDM student, example of valid student Number is: '08101'"+'\n\n'+" Please add a trailing 'D' to your student number if you are diploma student. Example: '0810056D' for CSE Diploma students"+'\n\n'+"*Note: If you are trying to enter a valid St. No and still being denied, please CONTACT the LAB ADMIN");
			return false;
		}
	}
	
	else
	{
		alert("Invalid Student Number. It should be of length 5, 7 or 8 characters"+'\n\n'+"*Note: If you are trying to enter a valid St. No and still being denied, please CONTACT the LAB ADMIN.")
		elem.focus();
		return false;
	}
}
</script>
<style type="text/css">
.smalltext{
    font-size: 10pt;
}
</style>

</head>

<body onLoad="document.getElementById('st_no').focus()">

<br>
<br>
<br>
<br>
<br>

<form onsubmit='return formValidator();' method="POST" action="student_entry_validation.php">

<table width="60%" height="74">
  <tr>
    <td width="320" style="border-style: none; border-width: medium" height="17" align="right">
    Enter your Student Number:</td>
    <td width="1000" style="border-style: none; border-width: medium" height="17">
    <input type="text" name="st_no" id="st_no" maxlength="8">
    </td>
  </tr>
  <tr>
    <td width="320" style="border-style: none; border-width: medium" height="17" align="right">
    Password:</td>
    <td width="1000" style="border-style: none; border-width: medium" height="17">
    <input type="text" style="font-style:italic;" name="password" id="password" value="Blank for first time" onfocus="if(this.value=='Blank for first time') {this.value=''; this.type='password';this.style.fontStyle='normal';}" onblur="if(this.value=='') {this.value='Blank for first time'; this.type='text';this.style.fontStyle='italic';}" maxlength="50">
    </td>
  </tr>
  <tr>
    <td width="320" style="border-style: none; border-width: medium" height="17" align="right">
    </td>
    <td width="1000" style="border-style: none; border-width: medium" height="17">
    <input type="submit" value="submit">
    </td>
  </tr>
</table>


</form>
<table width="47%" height="74">
  <tr>
    <td width="320" style="border-style: none; border-width: medium" height="17">
	<input type="button" value="Home" name="Home" onClick="location.href='<?echo $base."index.php"?>'">
    </td>
    <td width="70%" style="border-style: none; border-width: medium" height="17">
    </td>
  </tr>
</table>


</body>
</html>