LMS v1.5.3

-----------------------------
changelog v1.5.3
-----------------------------

1) applied client server technology by determining the computer numbers using ip addresses
2) optimised code in student_entry_validation.php line number 147
3) optimised code in student_entry_status.php line number 88
4) applied multiple queries initialization in one step on "active students login page"
5) fixed the problem of passing 2 d array in php post method
6) applied multiple unbanning of students in one step on "unban a student"
7) applied virtual host using port 80 


-----------------------------
changelog v1.5.2
-----------------------------

1) Changed date format in new student registrations.
2) Changed date format in stationary request.
3) Applied checkboxes and check all functionality in new students registrations.
4) Applied checkboxes and check all functionality in stationary requests.
5) Applied checkboxes and check all functionality in password reset reqest.
6) Applied checkboxes and check all functionality in computer complaints.


-----------------------------
changelog v1.5.1
-----------------------------

Fixed:
1) Diploma login problem
2) The "logout not done when clicked on 'disable button' in active systems"
3) Systems are now coming by computer no in 'active systems in use'
4) Hyperlink system number to complete system information
5) Allow new student registration in 'Paper entry' and 'Computer Complaint'
6) Compatibility problem 'leave blank for first time' in password field