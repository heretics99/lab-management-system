<?php
session_start();

if (!isset($_SESSION['student_logged_in'])||$_SESSION['student_logged_in']!=$stno||!isset($_SESSION['student_session_timeout_start'])) 
{
     header("Location:index.php");
     die();     // just to make sure no scripts execute
}
if(!isset($_SESSION['student_session_timeout_start'])) {
             
             $_SESSION['student_session_timeout_start']=time();			 
       }  
else
{
		$session_timeout = 70; // (in sec)
       $session_duration = time() - $_SESSION['student_session_timeout_start'];
       if ($session_duration > $session_timeout && $flag==1) {
           
        unset($_SESSION['student_logged_in']);
		unset($_SESSION['student_session_timeout_start']);
			echo "your session has expired please relogin.";
			die();
           
               //header("Location: index.php?expired=yes");  // Redirect to Login Page
       } 
	   else if ($session_duration > $session_timeout) {
           
        unset($_SESSION['student_logged_in']);
		unset($_SESSION['student_session_timeout_start']);

           
               header("Location: index.php?expired=yes");  // Redirect to Login Page
       } 
	   
	   
}

?>