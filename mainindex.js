
function formValidator(){

//return false
	// Make quick references to our fields
	var stno = document.getElementById('stno');
	
	// Check each input in the order that it appears in the form!
	if(isStudentNo(stno)){
	return true;
	}
		
		
		return false;	
	
}

function isStudentNo(elem){
	var len=elem.value.length
	if(len==5)
	{
		var stnoPGDMExp = /^[01][0-9]{4}$/;
		if(elem.value.match(stnoPGDMExp)) 
		{
			return true;
		}
		else 
		{
			alert("Not a valid student number. "+'\n\n'+" If you are a BTech or MCA student, some examples the valid student Numbers are:"+'\n'+""+'\n'+" '0910034' 	for CSE"+'\n'+" '1113056' 	for IT"+'\n'+" '0631034D' 	for EC Diploma"+'\n'+" '0721117' 	for EN"+'\n'+" '0740130D' 	for ME Diploma"+'\n'+" '1032001' 	for Electronics & Instrumentation"+'\n\n'+" '1098001' 	for Mtech Automation & Robotics"+'\n'+" '1025001' 	for Mtech Electrical Power & Energy System"+'\n'+" '1031001M' 	for Mtech Electronics & Comm"+'\n'+" '1010001M' 	for Mtech Computer Science"+'\n\n'+" '0814056' 	for MCA"+'\n\n'+" If you are a PGDM student, example of valid student Number is: '08101'"+'\n\n'+" Please add a trailing 'D' to your student number if you are diploma student. Example: 0810056D for CSE Diploma students"+'\n\n'+"*Note: If you are trying to enter a valid St. No and still being denied, please CONTACT the LAB ADMIN");
			return false;
		}
	}

	else if(len==7)
	{
		var stnoBTECHExp1 = /^[01][0-9](10|13|21|31|40|14|32)[0-9]{3}$/; //btech & mca
		//var stnoBTECHExp2 = /^[01][0-9](10|13|21|31|40|14)[0-9]{2}M$/;
		var stnoBTECHExp3 = /^[01][0-9](98|25|)[0-9]{3}$/; //mca without M
		if(elem.value.match(stnoBTECHExp1) || elem.value.match(stnoBTECHExp3))
		{
			return true;
		}
		else
		{
			alert("Not a valid student number. "+'\n\n'+" If you are a BTech or MCA student, some examples the valid student Numbers are:"+'\n'+""+'\n'+" '0910034' 	for CSE"+'\n'+" '1113056' 	for IT"+'\n'+" '0631034D' 	for EC Diploma"+'\n'+" '0721117' 	for EN"+'\n'+" '0740130D' 	for ME Diploma"+'\n'+" '1032001' 	for Electronics & Instrumentation"+'\n\n'+" '1098001' 	for Mtech Automation & Robotics"+'\n'+" '1025001' 	for Mtech Electrical Power & Energy System"+'\n'+" '1031001M' 	for Mtech Electronics & Comm"+'\n'+" '1010001M' 	for Mtech Computer Science"+'\n\n'+" '0814056' 	for MCA"+'\n\n'+" If you are a PGDM student, example of valid student Number is: '08101'"+'\n\n'+" Please add a trailing 'D' to your student number if you are diploma student. Example: 0810056D for CSE Diploma students"+'\n\n'+"*Note: If you are trying to enter a valid St. No and still being denied, please CONTACT the LAB ADMIN");
			return false;
		}
	}
	
	else if(len==8)
	{
		var stnoBTECHDipExp = /^[01][0-9](10|13|21|31|40|32)[0-9]{3}D$/; //for BTech Diploma
		var stnoMTechCseEce = /^[01][0-9](10|13|21|31|40|32)[0-9]{3}M$/; //for Mtech CSE and ECE only, as they have same branch codes(10,31) as Btech. The only diff is a trailing 
		
		if(elem.value.match(stnoBTECHDipExp)) 
		{
			return true;
		}
		if(elem.value.match(stnoMTechCseEce)) 
		{
			return true;
		}
		else
		{
			alert("Not a valid student number. "+'\n\n'+" If you are a BTech or MCA student, some examples the valid student Numbers are:"+'\n'+""+'\n'+" '0910034' 	for CSE"+'\n'+" '1113056' 	for IT"+'\n'+" '0631034D' 	for EC Diploma"+'\n'+" '0721117' 	for EN"+'\n'+" '0740130D' 	for ME Diploma"+'\n'+" '1032001' 	for Electronics & Instrumentation"+'\n\n'+" '1098001' 	for Mtech Automation & Robotics"+'\n'+" '1025001' 	for Mtech Electrical Power & Energy System"+'\n'+" '1031001M' 	for Mtech Electronics & Comm"+'\n'+" '1010001M' 	for Mtech Computer Science"+'\n\n'+" '0814056' 	for MCA"+'\n\n'+" If you are a PGDM student, example of valid student Number is: '08101'"+'\n\n'+" Please add a trailing 'D' to your student number if you are diploma student. Example: 0810056D for CSE Diploma students"+'\n\n'+"*Note: If you are trying to enter a valid St. No and still being denied, please CONTACT the LAB ADMIN");
			return false;
		}
	}
	
	else
	{
		alert("Invalid Student Number. It should be of length 5, 7 or 8 characters"+'\n\n'+"*Note: If you are trying to enter a valid St. No and still being denied, please CONTACT the LAB ADMIN.")
		elem.focus();
		return false;
	}
}

function isNumeric(elem, helperMsg){
	var numericExpression = /^[0-9]+$/;
	if(elem.value.match(numericExpression)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}


