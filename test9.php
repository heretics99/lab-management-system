<head>
<STYLE type="text/css">
OPTION.mar{background-color:maroon; color:white;}
OPTION.white{background-color:white; color:maroon}
</STYLE>
</head>
<body>
<FORM>

<SELECT>

<OPTION>What is your preferred browser?</OPTION>
<OPTION class="mar">Explorer 5.5</OPTION>
<OPTION class="white">Explorer 6.0</OPTION>
<OPTION class="mar">Netscape 4.7</OPTION>
<OPTION class="white">Netscape 6.0</OPTION>
<OPTION class="mar">WebTV</OPTION>
<OPTION class="white">Lynx</OPTION>
<OPTION class="mar">Other</OPTION>
</SELECT>
</FORM>

<form action="http://example.com/prog/someprog" method="post">
    <label for="food">What is your favorite food?</label>
    <select id="food" name="food">
      <optgroup label="Fruits">
        <option value="1">Apples</option>
        <option value="3">Bananas</option>
        <option value="4">Peaches</option>
        <option value="5">...</option>
      </optgroup>
      <optgroup label="Vegetables">
        <option value="2">Carrots</option>
        <option value="6">Cucumbers</option>
       <option value="7">...</option>
     </optgroup>
     <optgroup label="Baked Goods">
       <option value="8">Apple Pie</option>
       <option value="9">Chocolate Cake</option>
       <option value="10">...</option>
     </optgroup>
   </select>
</form>



<form action="http://example.com/prog/someprog" method="post">
    <label for="related_techniques"><strong>Related Techniques:</strong></label>
    <select name="related_techniques" id="related_techniques" multiple="multiple" size="10">
  <optgroup label="General Techniques">
    <option value="G1">G1: Adding a link at the top of each page ... </option>
    <option value="G4">G4: Allowing the content to be paused and restarted ... </option>
    <option value="G5">G5: Allowing users to complete an activity without any time... </option>
    <option value="G8">G8: Creating an extended audio description for the ... </option>
    <option value="G9">G9: Creating captions for live synchronized media... </option>
    <option value="G10">G10: Creating components using a technology that ... </option>
  </optgroup>
  <optgroup label="HTML Techniques">
    <option value="H2">H2: Combining adjacent image and text links for the same ... </option>
    <option value="H4">H4: Creating a logical tab order through links, form ... </option>
    <option value="H24">H24: Providing text alternatives for the area ...
    </option>
  </optgroup>
  <optgroup label="CSS Techniques">
    <option value="C6">C6: Positioning content based on structural markup... </option>
    <option value="C7">C7: Using CSS to hide a portion of the link text... </option>
  </optgroup>
  <optgroup label="SMIL Techniques">
    <option value="SM1">SM1: Adding extended audio description in SMIL 1.0... </option>
    <option value="SM2">SM2: Adding extended audio description in SMIL 2.0... </option>
    <option value="SM6">SM6: Providing audio description in SMIL 1.0... </option>
  </optgroup>
  <optgroup label="ARIA Techniques">
    <option value="ARIA1">ARIA1: Using WAI-ARIA describedby... </option>
    <option value="ARIA2">ARIA2: Identifying required fields with the "required"... </option>
    <option value="ARIA3">ARIA3: Identifying valid range information with "valuemin" ... </option>
    <option value="ARIA4">ARIA4: Using WAI-ARIA to programmatically identify form  ... </option>
  </optgroup>
  <optgroup label="Common Failures">
    <option value="F1">F1: Failure of SC 1.3.2 due to changing the meaning of content by... </option>
    <option value="F2">F2: Failure of SC 1.3.1 due to using changes in text presentation... </option>
    <option value="F3">F3: Failure of SC 1.1.1 due to using CSS to include images  ... </option>
    <option value="F4">F4: Failure of SC 2.2.2 due to using text-decoration:blink ...</option>
  </optgroup>
